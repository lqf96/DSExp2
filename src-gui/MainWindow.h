#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QUrl>
#include <QResizeEvent>
#include <QWebFrame>
#include <QWebPage>

#include "WebAppNI.h"
#include "Hashtable.h"

//UI namespace
namespace Ui
{
    class MainWindow;
}

//Main window class
class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    //Web application native interface object
    WebAppNI* NativeInterface;
public:
    //Constructor
    explicit MainWindow(QWidget *parent = 0);
    //Destructor
    ~MainWindow();
protected:
    //Resize event
    void resizeEvent(QResizeEvent*);
//Slots
private slots:
    //Show about message
    void showAbout();
private:
    //Form UI item
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
