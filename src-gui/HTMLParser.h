#pragma once
#include <fstream>
#include <functional>
#include "Array.h"
#include "_String.h"

//Debug switch
//#define EXP1_DEBUG

//Experiment 1 namespace
namespace Exp1
{
    //Class declaration
    class HTMLParser;
    class HTMLElement;

	//HTML parser
	class HTMLParser
	{private:
		//HTML node object
		class HTMLNode
		{public:
			//Node type
			enum HTMLNodeType
			{
				Text = 0,
				Prop,
				PropValue,
				Element,
				__Internal
			} Type;
			//Values
			WString Value;

			//Left & right child node
			HTMLNode* LNode;
			HTMLNode* RNode;

			//Easy constructor
			HTMLNode(HTMLNodeType _Type) : LNode(nullptr), RNode(nullptr), Type(_Type) {}
		};

		//HTML root node
		HTMLNode* RootNode;
		
		//HTML parser core
		static void HTMLParserCore(unsigned int& i, WString HTML, HTMLNode* Node);
		//Destroy HTML node tree
		static void DestroyNodeTree(HTMLNode* Node);
		//Iterate through a tree
		static void Iterate(HTMLNode* Node, std::function<void(HTMLNode*)> Func);

#ifdef EXP1_DEBUG
		//Recursively print all HTML nodes with proper indentation
		static void __IteratePrint(HTMLNode* Node, unsigned int Level);
#endif

		//Friend classes
		friend class HTMLElement;
	public:
		//Constructor
		HTMLParser(String Path);
		//Destructor
		~HTMLParser();

		//Get elements by tag name
		Array<HTMLElement> GetElementsByTagName(WString TagName);
		//Get elements by class name
		Array<HTMLElement> GetElementsByClassName(WString ClassName);

#ifdef EXP1_DEBUG
		//Recursively print all HTML nodes with proper indentation (Core function)
		void IteratePrint();
#endif
	};

	//HTML element class
	class HTMLElement
	{
	private:
		//HTML document the element belongs to
		HTMLParser* Document;
		//Element HTML node
		HTMLParser::HTMLNode* Node;

		//Private constructor
		HTMLElement(HTMLParser* _Doc, HTMLParser::HTMLNode* _Node);
		
		//Friend classes
		friend class HTMLParser;
	public:
		//Constructor
		HTMLElement();

		//Get inner text
		WString InnerText() const;
		//Get property name
		WString Attribute(WString Name, bool* Found = nullptr) const;
		//Get child elements
		Array<HTMLElement> ChildElements() const;
	};
}
