#include "WordMatching.h"
#include <string>
//SBT node searching
Exp1::SBTNode* Exp1::Search(Exp1::SBTNode* T, Exp1::SBTNode* Node, wchar_t Char)
{
	if (Node->Char == Char)
		return Node;

	SBTNode* Result = nullptr;
	if ((Char < Node->Char) && (Node->LNode))
		Result = Search(T, T + Node->LNode, Char);
	else if (Node->RNode)
		Result = Search(T, T + Node->RNode, Char);
	return Result;
}

//Read "dict2" dictionary from file
Exp1::SBTNode* Exp1::ReadDict2(Exp1::String Path, Exp1::String& Dict2Memory)
{
	//Open file
	std::ifstream Dict2FileIn(Path, std::ios::in | std::ios::binary);
	if (!Dict2FileIn.good())
		throw "FailedToOpenFile";
	//Read "dict2" file
	String __Dict2Memory;
	GetAll(Dict2FileIn, __Dict2Memory);
	Dict2FileIn.close();
	if (__Dict2Memory.SubStr(0, 5) != "dict3")
		throw "IllegalDict2File|IncompatibleDict2File";
	
	//Get SBT node pointer and return
	const char* __SBTEntry = &(__Dict2Memory.operator[](0));
	__SBTEntry += 5;
	Dict2Memory = __Dict2Memory;
	return ((SBTNode*)__SBTEntry);
}

//Do word spliting and matching
Exp1::LinkedList<Exp1::WString> Exp1::DoMatching(Exp1::SBTNode* T, Exp1::WString Paragraph)
{
	LinkedList<WString> Result;
	WString CurrentWord;
	unsigned int CurrentPos = 0;
	SBTNode* CTreeRoot = T;
	unsigned int i = 0;
	
	while (true)
	{
		auto CharNode = Search(T, T + CTreeRoot->ChildRoot, (i >= Paragraph.Size()) ? L'\0' : Paragraph[i]);
		if ((i >= Paragraph.Size()) && CurrentWord.Empty())
			break;

		if (CharNode)
		{
			CurrentWord += Paragraph[i];
			if (CharNode->WordEnd)
				CurrentPos = CurrentWord.Size();

			if (CharNode->ChildRoot != 0)
			{
				CTreeRoot = T + CharNode->ChildRoot;
			}
			else
			{
				if (CurrentPos != 0)
					Result.Push(CurrentWord.SubStr(0, CurrentPos));
				i -= CurrentWord.Size() - CurrentPos;

				CurrentWord = L"";
				CurrentPos = 0;
				CTreeRoot = T;
			}
		}
		else if (CurrentWord.Empty())
		{
			i++;
			continue;
		}
		else
		{
			if (CurrentPos != 0)
				Result.Push(CurrentWord.SubStr(0, CurrentPos));
			i -= CurrentWord.Size() - CurrentPos;

			CurrentWord = L"";
			CurrentPos = 0;
			CTreeRoot = T;
		}

		i++;
	}

	return Result;
}