$(function()
{
    //[ Javascript extension ]
    //Append text to element
    $.fn.appendText = function(text)
    {   return $(this).append(document.createTextNode(text));
    };
    //String join function
    String.prototype.join = function(list)
    {   var result = "";
        if (list.length>0)
        {   for (var i=0;i<list.length-1;i++)
                result += list[i]+this;
            result += list[list.length-1];
        }
        return result;
    };
    
    //[ Routing handler ]
    //Hash change handler
    $(window).on("hashchange", function()
    {   //Get address hash
        var raw_hash = location.hash.substr(1);
        var split_hash = raw_hash.split(":");
        var i;
        
        //Parse route params
        var route_params = {};
        if (split_hash.length>2)
        {   var split_params = split_hash[2].split("&");
            
            for (i=0;i<split_params.length;i++)
            {   var split_single = split_params[i].split("=");
                if (split_single.length==1)
                    route_params[split_single[0]] = "";
                else
                    route_params[split_single[0]] = split_single[1];
            }
        }
        //Save route params
        $(window).data("params", route_params);
        //Load given page to given element
        $("#"+split_hash[0]).load(split_hash[1]);
        //Print URL
        console.log("[Router] "+window.location.hash);
    });
    
    //Load given view in given element
    function load_view(element_id, view_url, params)
    {   //Save route params
        if (params)
            $(window).data("params", params);
        //Load given page
        $("#"+element_id).load(view_url+"?_=12345");
    }

    //[ App initialization ]
    //Load songs list
    $(window).data("songs_list", Exp2NI.GetSongsList());
    //Load songs list view in navigation panel
    load_view("nav-panel", "views/songs-list.html");
    //Load welcome view in main panel
    load_view("main-panel", "views/welcome.html");
});
