$(function()
{   //Global function object
    var exp2 = $(window).data("exp2");
    //Loop counter
    var i;
    
    //Load songs list
    var songs_list = $(window).data("songs_list");
    if (!songs_list)
    {   songs_list = Exp2NI.GetSongsList();
        $(window).data("songs_list", songs_list);
    }

    //Display songs list
    for (var song_id in songs_list)
        //<li class="list-group-item"></li>
        $("#songs-list").append($("<li />")
            .addClass("list-group-item")
            //<a href="views/song_detail.html?id=[Song ID]" target="main-panel"></a>
            .append($("<a />")
                .attr("href", "#main-panel:views/song-detail.html:id="+song_id)
                //[Title] - [Singer]
                .text(songs_list[song_id].Title+" - "+songs_list[song_id].Singer)));
});
