//Script loading
console.log("[Search] Script loaded.");
//Do initialization
$(function()
{
    //Begin initialization
    console.log("[Search] Begin initialization.");

    //Songs list
    var songs_list = $(window).data("songs_list");
    //Params
    var params = $(window).data("params");

    //Search by navigation bar
    $("#search-by-nav").children().on("click", function()
    {   $("#search-by-nav").children().removeClass("active");
        $(this).addClass("active");
        $(window).data("search_by", $(this).attr("search-by"))
    });

    //Reset search condition
    $("#btn-reset").on("click", function()
    {   $("#search-cond").val("");
        //Clear button clicked
        console.log("[Search] Clear button clicked.");
    });

    //Do searching
    $("#btn-search").on("click", function()
    {   //Check if search condition is empty
        if ($("#search-cond").val()=="")
        {   window.alert("搜索条件为空，请重新输入！");
            return;
        }

        //Search button clicked
        console.log("[Search] Search button clicked.");
        //Do searching
        window.location.hash = "#main-panel:views/search.html:do_search"
            +"&by="+$(window).data("search_by")
            +"&cond="+btoa(escape($("#search-cond").val()));
    });

    //No do search flag
    if (!("do_search" in params))
    {   //Clear current result panel
        $("#search-result-panel").append($("<p />")
            .text("还没有进行任何搜索！"));
        //Current search condition type
        $(window).data("search_by", "title");
        $("#search-by-nav [search-by='title']").addClass("active");

        //No search condition found
        console.log("[Search] No search condition found.")
    }
    else
    {   //Current search condition type
        $(window).data("search_by", params.by);
        $("#search-by-nav [search-by='"+params.by+"']").addClass("active");

        //Search condition found
        console.log("[Search] Search condition found.")

        //Search functions
        var search_funcs = [];
        //Search by album name
        search_funcs.album = function(album_name)
        {   var result = []
            for (var id in songs_list)
            {   if (songs_list[id].Album==album_name)
                    result.push(id);
            }
            return result;
        }
        //Search by singer name
        search_funcs.singer = function(singer_name)
        {   var result = []
            for (var id in songs_list)
            {   if (songs_list[id].Singer==singer_name)
                    result.push(id);
            }
            return result;
        }
        //Search by song name
        search_funcs.title = function(song_name)
        {   for (var id in songs_list)
            {   if (songs_list[id].Title.match(song_name))
                    return [id];
            }
            return [];
        }
        //Search by words
        search_funcs.words = Exp2NI.Search;
        //(Result content handler)
        search_funcs.words.result_handler = function(element)
        {   var prev_href = element.attr("href");
            var encoded_cond = btoa(escape($("#search-cond").val()));
            element.attr("href", prev_href+"&words_search_cond="+encoded_cond);
            return element;
        }

        //Append content to search table
        $("#search-result-panel").append($("<h3 />")
                .text("搜索结果"))
            .append($("<p />")
                .text("点击歌曲名称以了解更多信息与寻找搜索歌词。"))
            .append($("<table />")
                .addClass("table")
                .addClass("table-hover")
                .append($("<thead />")
                    .append($("<tr />")
                        .append($("<th />")
                            .text("歌曲名称")
                            .attr("width", "50%"))
                        .append($("<th />")
                            .text("歌手")
                            .attr("width", "25%"))
                        .append($("<th />")
                            .text("专辑")
                            .attr("width", "25%"))))
                .append($("<tbody />")
                    .attr("id", "search_result")
                    .addClass("table")
                    .addClass("table-hover")));

        //Get search condition
        var cond = unescape(atob(params.cond));
        //Fill search condition input
        $("#search-cond").val(cond);

        //Do searching
        var search_func = search_funcs[$(window).data("search_by")];
        var result = search_func(cond);
        console.log("[Search] Result size: "+result.length);

        //Show search result
        for (var i=0;i<result.length;i++)
        {   var id = result[i];
            //Print ID and song info object
            console.log("[Search] Result ID: "+id);
            console.log("[Search] Current song: "+JSON.stringify(songs_list[id]));

            //Link to song details
            var song_link = $("<a />")
                .attr("href", "#main-panel:views/song-detail.html:id="+id)
                .text(songs_list[id].Title);
            if (search_func.result_handler)
                song_link = search_func.result_handler(song_link);

            //Show result entry
            $("#search_result").append($("<tr />")
                .append($("<td />")
                    .append(song_link))
                .append($("<td />")
                    .text(songs_list[id].Singer))
                .append($("<td />")
                    .text(songs_list[id].Album)));
        }
    }
});

//@ sourceURL=qrc:/DSExp2_Web/js/search.js
