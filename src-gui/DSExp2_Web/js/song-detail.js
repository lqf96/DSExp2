$(function()
{   //Loop counter
    var i;

    //Read song ID
    var id = $(window).data("params").id;
    //Load song information
    var song_info = Exp2NI.GetSongInfo(id);

    //Debug: Song detail script
    console.log("[Song Detail] ID = "+id);
    
    //Show song information
    //Title
    $("#song-title").text(song_info.Title);
    //Singer
    $("#song-basic-info").append($("<tr />")
        .append($("<td />")
            .attr("width", "25%")
            .text("歌手"))
        .append($("<td />")
            .text(song_info.Singer)));
    //Composer
    $("#song-basic-info").append($("<tr />")
        .append($("<td />")
            .text("作曲"))
        .append($("<td />")
            .text(song_info.Composer)));
    //Lyrics maker
    $("#song-basic-info").append($("<tr />")
        .append($("<td />")
            .text("作词"))
        .append($("<td />")
            .text(song_info.LyricsMaker)));
    //Album
    $("#song-basic-info").append($("<tr />")
        .append($("<td />")
            .text("专辑"))
        .append($("<td />")
            .text(song_info.Album)));
    //Release Time
    $("#song-basic-info").append($("<tr />")
        .append($("<td />")
            .text("发行日期"))
        .append($("<td />")
            .text(song_info.ReleaseTime)));
    //Lyrics
    for (i=0;i<song_info.Lyrics.length;i++)
        $("#song-lyrics")
            .appendText(song_info.Lyrics[i])
            .append($("<br />"));

    //Get recommendation result
    var recommends = Exp2NI.Recommend(id);
    //Songs info list
    var songs_list = $(window).data("songs_list");
    //Show recommend song title and singer
    for (i=0;i<recommends.length;i++)
        $("#song-similar").appendText("+ ")
            .append($("<a />")
                .attr("href", "#main-panel:views/song-detail.html:id="+recommends[i])
                .text(songs_list[recommends[i]].Title+" - "+songs_list[recommends[i]].Singer))
            .append("<br />");

    //Highlight searching words in words search mode
    if ("words_search_cond" in $(window).data("params"))
    {   var raw_cond = unescape(atob($(window).data("params").words_search_cond));
        var split_raw_cond = raw_cond.split(' ');
        var cond_list = [];

        //Do word matching
        for (var i=0;i<split_raw_cond.length;i++)
            cond_list = cond_list.concat(Exp2NI.DoMatching(split_raw_cond[i]));

        //Show words searching result info
        $("#main-panel").prepend($("<div />")
                .addClass("alert")
                .addClass("alert-info")
                .appendText("本次查询的歌词搜索关键字："+" ".join(cond_list))
                .append("<br />")
                .appendText("所有关键字均高亮显示。"))
            .prepend($("<div />")
                .addClass("separator"));

        //Highlight all query words
        var lyrics_html = $("#song-lyrics").html();
        for (var i=0;i<cond_list.length;i++)
            lyrics_html = lyrics_html.replace(new RegExp(cond_list[i], "g"),
                "<highlight>"+cond_list[i]+"</highlight>");
        $("#song-lyrics").html(lyrics_html);
    }
});
