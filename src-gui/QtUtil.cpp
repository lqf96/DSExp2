#include "QtUtil.h"

//Experiment 2 GUI namespace
namespace Exp2
{
    //Build document dictionary (Enhanced, cache-supported version for Qt)
    void BuildDocDict2(Exp1::HashTable<Exp1::String, Exp1::String> Settings,
        Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem>& DocDict,
        Exp2::CurrentHashTable<unsigned int, Exp1::SongInfo>& SongsInfo,
        Exp1::SBTNode*& DictRoot,
        Exp1::String& Dict2Memory)
    {
        //Read dictionary from given address
        DictRoot = Exp1::ReadDict2(Settings["dict"], Dict2Memory);

        //Use cache or not
        bool UseCache = false;
        //Web page directory
        auto WebPageDir = Settings["webpage-dir"];
        //Document directory
        Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem> _DocDict;
        //Songs info hash table
        Exp2::CurrentHashTable<unsigned int, Exp1::SongInfo> _SongsInfo(Exp1::UIntHash);

        //Check cache
        std::ifstream CacheIn(WebPageDir + ".ddc", std::ios::binary);
        //Last modified time from cache
        long long CacheLMD;
        //Last modified date
        long long LastModifyDate;

        //Cache file found
        if (CacheIn.good())
            //Read cache content and last modified date
            _DocDict = Exp2::DeserializeDocDict(CacheIn, CacheLMD, &_SongsInfo);
        CacheIn.close();

        struct _stat DirStat;
        //Try to fetch last modified date
        if (_stat(WebPageDir, &DirStat) == 0)
        {
            LastModifyDate = DirStat.st_mtime;
            //Folder content not changed, use cache
            if ((CacheIn.good()) && (LastModifyDate == CacheLMD))
            {
                DocDict = _DocDict;
                SongsInfo = _SongsInfo;
                UseCache = true;
            }
        }

        //Cache not used, build document dictionary and cache
        if (!UseCache)
        {
            //Build document dictionary
            DocDict = Exp2::BuildDocDict(DictRoot, WebPageDir, &SongsInfo);
            //Write dictionary content to cache file
            std::ofstream CacheOut(WebPageDir + ".ddc", std::ios::binary);
            if (CacheOut.good())
                Exp2::SerializeDocDict(CacheOut, DocDict, LastModifyDate, &SongsInfo);
            CacheOut.close();
        }
    }

    //[ String Utilities ]
    //Convert to QString
    QString ToQString(Exp1::WString Str)
    {
        //Use std::wstring as bridge
        std::wstring _Str;
        unsigned int i;

        //Generate std::wstring
        for (i=0;i<Str.Size();i++)
            _Str += Str[i];
        //Convert to QString
        return QString::fromStdWString(_Str);
    }

    //Convert to WString
    Exp1::WString ToWString(QString Str)
    {
        //Use std::wstring as bridge
        std::wstring _Str = Str.toStdWString();
        Exp1::WString Result;
        unsigned int i;

        //Generate Exp1::WString
        for (i=0;i<_Str.size();i++)
            Result += _Str[i];
        return Result;
    }
}
