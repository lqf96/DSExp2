#pragma once

#include <functional>

//Experiment 1 namespace
namespace Exp1
{	
	//Default array capacity
	const unsigned int __DefaultArrayCapacity = 4;
	
	//Array class
	template <typename T>
	class Array
	{private:
		//Internal array
		T* __Array;
		//Array size
		unsigned int __Size;
		//Array capacity
		unsigned int __Capacity;
		//Reference count
		unsigned int* RefCount;

		//Clone internal array
		void Clone()
		{
			//New internal array
			T* NewIArray = new T[this->__Capacity];
			unsigned int* NewRefCount = new unsigned int(1);
			unsigned int i;

			//Copy data
			for (i = 0; i < this->__Capacity; i++)
				NewIArray[i] = this->__Array[i];
			//Update reference count
			(*this->RefCount)--;

			//Set new internal array and reference count
			this->__Array = NewIArray;
			this->RefCount = NewRefCount;
		}

		//Expand capacity
		void Expand()
		{
			//New internal array
			T* NewIArray = new T[this->__Capacity * 2];
			unsigned int i;

			//Copy data and delete old array
			for (i = 0; i < this->__Size; i++)
				NewIArray[i] = this->__Array[i];
			delete[] this->__Array;

			//Set new internal array and reference count
			this->__Array = NewIArray;
			this->__Capacity *= 2;
		}

		//Shrink capacity
	public:
		//Array iterator
		class Iterator
		{private:
			//Current index
			unsigned int __Index;
			//Array reference
			Array<T>* __Array;
		public:
			//Public constructor (From array and index)
			Iterator(unsigned int _Index, const Array<T>* _Array)
			{
				this->__Index = _Index;
				this->__Array = const_cast<Array<T>*>(_Array);
			}

			//Get index
			unsigned int Index() const
			{
				return this->__Index;
			}

			//Get array
			Array<T>* Container() const
			{
				return this->__Array;
			}

			//Get data
			const T& operator *() const
			{
				return this->__Array->operator[](this->__Index);
			}

			//Get data
			const T* operator ->() const
			{
				return &(this->__Array->operator[](this->__Index));
			}

			//Update data
			T& Data()
			{
				return this->__Array->At(this->__Index);
			}

			//Move to next item
			Iterator operator ++()
			{
				if (this->__Index > this->__Array->Size())
					throw "EndOfArray";
				this->__Index++;
				return *this;
			}

			//Move to next item
			Iterator operator ++(int)
			{
				if (this->__Index > this->__Array->Size())
					throw "EndOfArray";

				Iterator NewIterator(*this);
				this->__Index++;
				return NewIterator;
			}

			//Move to previous item
			//(Index checking are not implemented due to unsigned index problem. Using with caution!)
			Iterator operator --()
			{
				this->__Index--;
				return *this;
			}

			//Move to previous item
			Iterator operator --(int)
			{
				Iterator NewIterator(*this);
				this->__Index--;
				return NewIterator;
			}

			//Compare item
			bool operator ==(Iterator Ptr) const
			{
				return (this->__Index == Ptr.__Index) && (this->__Array == Ptr.__Array);
			}

			//Compare item
			bool operator !=(Iterator Ptr) const
			{
				return (this->__Index != Ptr.__Index) || (this->__Array != Ptr.__Array);
			}
		};

		//Constructor
		Array()
		{
			this->__Array = new T[__DefaultArrayCapacity];
			this->__Capacity = 4;
			this->__Size = 0;
			this->RefCount = new unsigned int(1);
		}

		//Copy constructor
		Array(const Array<T>& _Array)
		{
			this->__Array = nullptr;
			this->__Capacity = 4;
			this->__Size = 0;
			this->RefCount = nullptr;

			//Use "=" operator when copying
			*this = _Array;
		}

		//Destructor
		~Array()
		{
			(*this->RefCount)--;
			//No reference pointing to internal array
			if (*this->RefCount == 0)
			{
				delete[] this->__Array;
				delete this->RefCount;
			}
		}

		//Return array size
		unsigned int Size() const
		{
			return this->__Size;
		}

		//Check if the array is empty
		bool Empty() const
		{
			return this->__Size == 0;
		}

		//Return array capacity
		unsigned int Capacity() const
		{
			return this->__Capacity;
		}

		//Overload operator "[]" (Read-only access)
		const T& operator [](unsigned int Index) const
		{
			if (Index < this->__Size)
				return this->__Array[Index];
			else
				throw "IndexOutOfBound";
		}

		//Read-write access to element
		T& At(unsigned int Index)
		{
			//Clone array if necessary
			if (*this->RefCount > 1)
				this->Clone();
			//Return reference
			return const_cast<T&>(this->operator[](Index));
		}

		//Get begin iterator
		Iterator Begin()
		{
			return Iterator(0, this);
		}

		//Get end iterator
		Iterator End()
		{
			return Iterator(this->__Size, this);
		}

		//Get reverse begin iterator
		Iterator RBegin()
		{
			return Iterator(this->__Size - 1, this);
		}

		//Get reverse end iterator
		//(Use -1 is not good approach, through)
		Iterator REnd()
		{
			return Iterator(-1, this);
		}

		//Get first item
		const T& First() const
		{
			if (this->__Size != 0)
				return this->__Array[0];
			else
				throw "EmptyArray";
		}

		//Get last item
		const T& Last() const
		{
			if (this->__Size != 0)
				return this->__Array[this->__Size - 1];
			else
				throw "EmptyArray";
		}

		//Push an item to the end of the array
		void Push(T Item)
		{
			if (this->__Size >= 1)
				this->Add(Item, this->__Size - 1);
			else
			{
				this->__Array[0] = Item;
				this->__Size = 1;
			}
		}

		//Push an item to the front of the array
		void FPush(T Item)
		{
			if (this->__Size >= 1)
				this->Add(Item, 0, false);
			else
			{
				this->__Array[0] = Item;
				this->__Size = 1;
			}
		}

		//Overload operator "+" for a single item
		Array<T> operator +(T Item) const
		{
			Array<T> NewArray = *this;
			NewArray.Push(Item);
			return NewArray;
		}

		//Overload operator "+=" for a single item
		Array<T>& operator +=(T Item)
		{
			this->Push(Item);
			return *this;
		}

		//Overload operator "+" for an array
		Array<T> operator +(const Array<T>& _Array) const
		{
			Array<T> NewArray = *this;
			unsigned int i;

			for (i = 0; i < _Array.__Size;i++)
				NewArray.Push(_Array[i]);
			return NewArray;
		}

		//Overload operator "+=" for an array
		Array<T>& operator +=(const Array<T>& _Array)
		{
			unsigned int i;

			for (i = 0; i < _Array.__Size; i++)
				this->Push(_Array[i]);
			return *this;
		}

		//Overload operator "=" for array
		Array<T>& operator =(const Array<T>& _Array)
		{
			//Copy capacity and size
			this->__Capacity = _Array.__Capacity;
			this->__Size = _Array.__Size;

			//Internal array not same
			if (this->RefCount != _Array.RefCount)
			{
				//Only one count to internal array, destroy internal array and reference count
				if ((this->RefCount) && (*this->RefCount == 1))
				{
					delete[] this->__Array;
					delete this->RefCount;
				}

				//Copy internal array and reference count
				this->__Array = _Array.__Array;
				this->RefCount = _Array.RefCount;
				//Update reference count
				(*this->RefCount)++;
			}

			return *this;
		}

		//Add an item to the array
		void Add(T Item, unsigned int Index, bool After = true)
		{
			unsigned int i;

			if (Index >= this->__Size)
				throw "IndexOutOfBound";

			//Clone internal array when sharing internal array
			if (*this->RefCount > 1)
				this->Clone();
			//Full internal array, expand
			if (this->__Size == this->__Capacity)
				this->Expand();

			//Move elements, then insert new element
			for (i = this->__Size - 1; i > Index ; i--)
				this->__Array[i + 1] = this->__Array[i];
			if (!After)
				this->__Array[Index + 1] = this->__Array[Index];
			this->__Array[After ? (Index + 1) : Index] = Item;

			//Update size
			this->__Size++;
		}

		//Pop an item from the end of the array
		T Pop()
		{
			return this->Remove(this->__Size - 1);
		}

		//Pop an item from the front of the array
		T FPop()
		{
			return this->Remove(0);
		}

		//Remove an item from the array
		T Remove(unsigned int Index)
		{
			unsigned int i;
			//Fetch removed item
			T RemovedItem = this->__Array[Index];

			if (Index >= this->__Size)
				throw "IndexOutOfBound";

			//Clone internal array when sharing internal array
			if (*this->RefCount > 1)
				this->Clone();

			//Move elements and update size
			for (i = Index + 1; i < this->__Size; i++)
				this->__Array[i - 1] = this->__Array[i];
			this->__Size--;

			return RemovedItem;
		}

		//Check if the array contains an item
		bool Contains(T Item, unsigned int* Index = nullptr) const
		{
			unsigned int i;

			for (i = 0; i < this->__Size;i++)
				if (this->__Array[i] == Item)
				{
					if (Index)
						*Index = i;
					return true;
				}
			return false;
		}

		//Get a sub-array from the array
		Array<T> SubArray(unsigned int Begin = 0) const
		{
			return this->SubArray(Begin, this->__Size);
		}

		//Get a sub-array from the array
		Array<T> SubArray(unsigned int Begin, unsigned int End) const
		{
			if ((Begin > End) || (End > this->__Size))
				throw "IndexOutOfBound";

			Array<T> Result;
			unsigned int i;

			for (i = Begin; i < End; i++)
				Result.Push(this->operator[](i));

			return Result;
		}

		//Iterate through the array
		void ForEach(std::function<void(T)> Func) const
		{
			unsigned int i;
			for (i = 0; i < this->__Size; i++)
				Func(this->__Array[i]);
		}

		//Hash function (Based on BKDR hash)
		unsigned int Hash()
		{
			unsigned int Seed = 131;
			unsigned int _Hash = 0;
			unsigned int i, j;

			for (i = 0; i < this->Size(); i++)
			{
				const unsigned char* _Ptr = (const unsigned char*)(&(this->operator[](i)));
				for (j = 0; j < sizeof(T); j++)
					_Hash = _Hash * Seed + _Ptr[j];
			}

			return _Hash;
		}
	};
}