#-------------------------------------------------
#
# Project created by QtCreator 2015-12-16T23:26:00
#
#-------------------------------------------------

QT       += core gui webkitwidgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = DSExp2_GUI
TEMPLATE = app

CONFIG += \
    c++11

SOURCES += \
    MainWindow.cpp \
    Main.cpp \
    HTMLElement.cpp \
    HTMLParser.cpp \
    MusicInfoFetcher.cpp \
    ReverseIndexMaker.cpp \
    ReverseIndexSerializer.cpp \
    SongRecommendation.cpp \
    String.cpp \
    Util.cpp \
    WordMatching.cpp \
    QtUtil.cpp \
    WebAppNI.cpp

HEADERS  += MainWindow.h \
    Algorithm.h \
    Array.h \
    BTreeHashtable.h \
    Hashtable.h \
    HTMLParser.h \
    LinkedList.h \
    MusicInfoFetcher.h \
    ReverseIndexMaker.h \
    SongRecommendation.h \
    Util.h \
    WordMatching.h \
    QtUtil.h \
    WebAppNI.h \
    _String.h

FORMS    += MainWindow.ui

RESOURCES += \
    webapp.qrc
