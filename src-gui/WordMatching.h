#pragma once

#include <fstream>
#include "Array.h"
#include "_String.h"
#include "LinkedList.h"
#include "WordMatching.h"

//Experiment 1 namespace
namespace Exp1
{
	//SBT node
	struct SBTNode
	{	//Character as key
		wchar_t Char;
		//Left and right node
		unsigned int LNode;
		unsigned int RNode;
		//Child tree root position
		unsigned int ChildRoot;
		//Indicate if the word ends here
		bool WordEnd;
	};

	//SBT node searching
	SBTNode* Search(Exp1::SBTNode* T, Exp1::SBTNode* Node, wchar_t Char);
	//Read "dict2" dictionary from file
	SBTNode* ReadDict2(String Path, Exp1::String& Dict2Memory);
	//Do word spliting and matching
	LinkedList<WString> DoMatching(SBTNode* T, WString Paragraph);
}
