#pragma once

#include <cstdlib>
#include <iostream>
#include "LinkedList.h"
#include "_String.h"
#include "HashTable.h"
#include "WordMatching.h"
#include "MusicInfoFetcher.h"
#include "Util.h"
#include "Algorithm.h"
#include "BTreeHashtable.h"

//Experiment 2 namespace
namespace Exp2
{
	//Single document word record item structure
	struct WordSingleDocItem
	{
		unsigned int DocID;
		unsigned int Count;
	};

	//Document dictionary structure
	struct DocDictItem
	{
		Exp1::WString Word;
		unsigned int WordID;
		unsigned int OccurInFileCount;
		unsigned int OccurCount;
		Exp1::LinkedList<WordSingleDocItem> DocsList;
	};

	//Search result item structure
	struct SearchResultItem
	{
		unsigned int DocID;
		unsigned int OccurWordCount;
		unsigned int Count;
	};

	//Current hash table implementation
	template <typename KT, typename VT>
    using CurrentHashTable = Exp2::BTreeHashTable<KT, VT>;

	//Build document dictionary for single document
	void BuildDocDictSingle(Exp1::LinkedList<Exp1::WString> MatchingResult,
		CurrentHashTable<Exp1::WString, DocDictItem>& DocDict,
		unsigned int DocID);
	//Build document dictionary from HTML files in given folder
	CurrentHashTable<Exp1::WString, DocDictItem> BuildDocDict(Exp1::SBTNode* Dict2Root,
		Exp1::String FolderPath,
		CurrentHashTable<unsigned int, Exp1::SongInfo>* SongsInfo = nullptr);
	//Do searching using document dictionary
	Exp1::Array<Exp1::Array<SearchResultItem>> DoSearching(Exp1::SBTNode* Dict2Root,
		CurrentHashTable<Exp1::WString, DocDictItem> DocDict,
		Exp1::WString QueryText);
	//Serialize search result
	Exp1::WString SerializeSearchResult(Exp1::Array<Exp1::Array<SearchResultItem>> SearchResult);
	
	//Serialize document dictionary
	void SerializeDocDict(std::basic_ostream<char>& Out,
		Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem>& DocDict,
		long long LastModifyDate,
		Exp2::CurrentHashTable<unsigned int, Exp1::SongInfo>*SongsInfo = nullptr);
	//Deserialize document dictionary
	CurrentHashTable<Exp1::WString, DocDictItem> DeserializeDocDict(std::basic_istream<char>& In,
		long long& LastModifyDate,
		Exp2::CurrentHashTable<unsigned int, Exp1::SongInfo>* SongsInfo = nullptr);
}
