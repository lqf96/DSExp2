#pragma once

#include <fstream>
#include "_String.h"
#include "HTMLParser.h"
#include "WordMatching.h"

namespace Exp1
{
	//Song information
	struct SongInfo
	{
		WString Title;
		WString Singer;
		WString Album;
		WString ReleaseTime;
		WString LyricsMaker;
		WString Composer;
		Array<Exp1::WString> Lyrics;
	};

	//Get music information from page
	SongInfo GetMusicInfoFromPage(String Path);
}
