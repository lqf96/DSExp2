#include "WebAppNI.h"

//Constructor
WebAppNI::WebAppNI(Exp1::HashTable<Exp1::String, Exp1::String> Settings)
    : SongsInfo(Exp1::UIntHash)
{
    //Build document dictionary
    Exp2::BuildDocDict2(Settings, this->DocDict, this->SongsInfo, this->Dict2Root, this->Dict2Memory);
}

//Get songs list
QVariantMap WebAppNI::GetSongsList()
{
    //Result
    QVariantMap Result;
    //Do convertion
    this->SongsInfo.ForEach([&](unsigned int DocID, Exp1::SongInfo Info)
    {
        QVariantMap _Info;
        //Title, album and singer name
        _Info["Title"] = Exp2::ToQString(Info.Title);
        _Info["Singer"] = Exp2::ToQString(Info.Singer);
        _Info["Album"] = Exp2::ToQString(Info.Album);
        //Save to result
        Result[QString::number(DocID)] = _Info;
    });
    //Return result
    return Result;
}

//Get song info
QVariantMap WebAppNI::GetSongInfo(int DocID)
{
    bool Exists;
    QVariantMap Result;
    Exp1::SongInfo Info = this->SongsInfo.Get(DocID, &Exists);

    //Song found
    if (Exists)
    {
        Result["Title"] = Exp2::ToQString(Info.Title);
        Result["Singer"] = Exp2::ToQString(Info.Singer);
        Result["Album"] = Exp2::ToQString(Info.Album);
        Result["ReleaseTime"] = Exp2::ToQString(Info.ReleaseTime);
        Result["Composer"] = Exp2::ToQString(Info.Composer);
        Result["LyricsMaker"] = Exp2::ToQString(Info.LyricsMaker);

        QStringList Lyrics;
        Info.Lyrics.ForEach([&](Exp1::WString SingleLyric)
        {
            Lyrics.push_back(Exp2::ToQString(SingleLyric));
        });
        Result["Lyrics"] = Lyrics;
    }
    //Not found
    else
        Result["__NotFound"] = true;

    return Result;
}

//Get search result
QList<int> WebAppNI::Search(QString Cond)
{
    //Search condition in WString format
    Exp1::WString RawCond = Exp2::ToWString(Cond);
    //Do searching
    auto RawResult = Exp2::DoSearching(this->Dict2Root, this->DocDict, RawCond);

    //Generate result
    QList<int> Result;
    RawResult[0].ForEach([&](Exp2::SearchResultItem Item)
    {   Result.push_back(Item.DocID);
    });
    //Return result
    return Result;
}

//Get recommendation information
QList<int> WebAppNI::Recommend(int DocID)
{
    //Do recommendation
    auto RawResult = Exp2::DoRecommendation(this->SongsInfo, this->SongsInfo[DocID].Title);
    //Generate result
    QList<int> Result;
    RawResult[0].ForEach([&](std::pair<unsigned int, Exp1::WString> RecommendSong)
    {
        Result.push_back(RecommendSong.first);
    });
    //Return result
    return Result;
}

//Do word matching
QStringList WebAppNI::DoMatching(QString Str)
{
    auto RawResult = Exp1::DoMatching(this->Dict2Root, Exp2::ToWString(Str));
    //Generate result
    QStringList Result;
    RawResult.ForEach([&](Exp1::WString Word)
    {   Result.push_back(Exp2::ToQString(Word));
    });
    //Return result
    return Result;
}
