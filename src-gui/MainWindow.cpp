#include "MainWindow.h"
#include "ui_mainwindow.h"

//Constructor
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    //Settings hash table
    Exp1::HashTable<Exp1::String, Exp1::String> Settings;
    //Configurations
    Settings.Put("dict", "../var/exp2_dict.dict3");
    Settings.Put("webpage-dir", "pages_300");

    //Set up UI
    ui->setupUi(this);

    auto webViewSettings = ui->webView->settings();
    //Allow running javascript
    webViewSettings->setAttribute(QWebSettings::JavascriptEnabled, true);
    //Disable context menu
    ui->webView->setContextMenuPolicy(Qt::NoContextMenu);

    //Initialize native interface
    this->NativeInterface = new WebAppNI(Settings);
    //Enable native interface
    QWebFrame* frame = ui->webView->page()->mainFrame();
    frame->addToJavaScriptWindowObject("Exp2NI", this->NativeInterface);

    //Show "app.html"
    ui->webView->setUrl(QUrl("qrc:/DSExp2_Web/app.html"));
}

//Destructor
MainWindow::~MainWindow()
{
    //Destroy UI
    delete ui;
    //Destroy native interface object
    delete this->NativeInterface;
}

//Show about message
void MainWindow::showAbout()
{
    QMessageBox::information(nullptr, "关于Exp2 GUI Demo",
        "Exp2 GUI Demo\n作者：鲁其璠（lqf.1996121@gmail.com）\n以GPL协议发布。");
}

//Window resize event
void MainWindow::resizeEvent(QResizeEvent*)
{
    ui->webView->resize(this->size());
}
