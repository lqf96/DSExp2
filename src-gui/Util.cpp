#include "Util.h"

//[ File Utilities ]
//Get the name of the files in a dictionary
Exp1::Array<Exp1::String> Exp1::GetFiles(Exp1::String Folder, Exp1::String FileNameFilter = "*")
{
	_finddata_t FolderC;
	Folder += '\\' + FileNameFilter;
	auto FileH = _findfirst(Folder, &FolderC);
	Array<String> Result;

	if (FileH == -1)
		throw "FailedToFetchFolderInfo";

	Result.Push(FolderC.name);
	while (_findnext(FileH, &FolderC) == 0)
	{
		//Ignore this directory and parent directory
		if ((strcmp(".", FolderC.name) == 0) || (strcmp("..", FolderC.name) == 0))
			continue;
		//Add file name to result
		Result.Push(FolderC.name);
	}

	return Result;
}

//[ Hashtable Utilities ]
unsigned int Exp1::UIntHash(unsigned int UInt)
{
	return UInt;
}