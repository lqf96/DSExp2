#include "MainWindow.h"
#include <QApplication>

//Program entry
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
