#pragma once

#include <iostream>
#include <fstream>
#include <sys/stat.h>
#include <functional>
#include <string>

#include "_String.h"
#include "ReverseIndexMaker.h"
#include "WordMatching.h"
#include "Hashtable.h"
#include "Util.h"
#include "SongRecommendation.h"

#include <QString>
#include <QMap>

//Experiment 2 namespace
namespace Exp2
{
    //Build document dictionary (Enhanced, cache-supported version for Qt)
    void BuildDocDict2(Exp1::HashTable<Exp1::String, Exp1::String> Settings,
        Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem>& DocDict,
        Exp2::CurrentHashTable<unsigned int, Exp1::SongInfo>& SongsInfo,
        Exp1::SBTNode*& DictRoot,
        Exp1::String& DictMemory);

    //[ String Utilities ]
    //Convert to QString
    QString ToQString(Exp1::WString Str);
    //Convert to WString
    Exp1::WString ToWString(QString Str);
}
