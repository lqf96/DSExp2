#pragma once

#include <utility>

#include <QObject>
#include <QMap>
#include <QList>
#include <QVariant>

#include "Util.h"
#include "QtUtil.h"
#include "ReverseIndexMaker.h"
#include "_String.h"
#include "Hashtable.h"
#include "WordMatching.h"

//Web App Native Interface Object
class WebAppNI : public QObject
{
    Q_OBJECT
private:
    //Songs information
    Exp2::CurrentHashTable<unsigned int, Exp1::SongInfo> SongsInfo;
    //Document dictionary
    Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem> DocDict;
    //SBT dictionary root
    Exp1::SBTNode* Dict2Root;
    //SBT dictionary memory
    Exp1::String Dict2Memory;
public:
    //Constructor
    WebAppNI(Exp1::HashTable<Exp1::String, Exp1::String> Settings);

    //Get songs list
    Q_INVOKABLE QVariantMap GetSongsList();
    //Get song info
    Q_INVOKABLE QVariantMap GetSongInfo(int DocID);
    //Get search result
    Q_INVOKABLE QList<int> Search(QString Cond);
    //Get recommendation information
    Q_INVOKABLE QList<int> Recommend(int DocID);
    //Do word matching
    Q_INVOKABLE QStringList DoMatching(QString Str);
};
