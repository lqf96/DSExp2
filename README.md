# Data Structure Course Experiment Phase II Demo

## Introduction
See [Experiment Report](doc/实验报告.pdf).

## Build
### CLI Program
For POSIX-compatible platform, simply `cd` into `src` directory and `make`.  
For Windows you will need VS2015 or later to build the project.
### GUI Program
Use Qt Creator or `qmake` to build the GUI Demo.

## License
The whold project is licensed under GNU GPLv3.
