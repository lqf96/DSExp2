#include "ReverseIndexMaker.h"

//Build document dictionary for single document
void Exp2::BuildDocDictSingle(Exp1::LinkedList<Exp1::WString> MatchingResult,
	Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem>& DocDict,
	unsigned int DocID)
{
	//Word match result iterator
	auto Ptr = MatchingResult.Begin();
	//Document intermidiate dict
	Exp2::CurrentHashTable<Exp1::WString, Exp2::WordSingleDocItem> DocIntDict;

	//Create document intermidiate dict
	while (Ptr != MatchingResult.End())
	{
		bool WordExists;
		//Check if the word exists
		Exp2::WordSingleDocItem& Item = DocIntDict.Data(*Ptr, &WordExists);
		
		//Word exists
		if (WordExists)
			Item.Count++;
		//Not exists, create a new item
		else
		{
			Exp2::WordSingleDocItem NewItem;
			NewItem.Count = 1;
			NewItem.DocID = DocID;
			DocIntDict.Put(*Ptr, NewItem);
		}

		Ptr++;
	}

	//Append result to document dictionary
	DocIntDict.ForEach([&DocDict](Exp1::WString Word, Exp2::WordSingleDocItem SingleDocItem)
	{
		bool WordExists;
		//Check if the word exists in the dictionary
		Exp2::DocDictItem& Item = DocDict.Data(Word, &WordExists);

		//Word exists
		if (WordExists)
		{
			Item.DocsList.Push(SingleDocItem);
			Item.OccurInFileCount++;
			Item.OccurCount += SingleDocItem.Count;
		}
		//Not exists
		else
		{
			Exp2::DocDictItem NewItem;
			NewItem.Word = Word;
			NewItem.WordID = DocDict.Size();
			NewItem.OccurCount = 1;
			NewItem.OccurInFileCount = SingleDocItem.Count;
			NewItem.DocsList.Push(SingleDocItem);
			DocDict.Put(Word, NewItem);
		}
	});
}

//Build document dictionary from given document file names
Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem> Exp2::BuildDocDict(Exp1::SBTNode* Dict2Root,
	Exp1::String FolderPath,
	Exp2::CurrentHashTable<unsigned int, Exp1::SongInfo>* SongsInfo)
{
	//Document dictionary object
	Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem> DocDict;
	//Loop counter
	unsigned int i, j;

	//Read all HTML file names
	Exp1::Array<Exp1::String> HTMLFileNames = Exp1::GetFiles(FolderPath, "*.html");
	//Start building dictionary
	for (i = 0; i < HTMLFileNames.Size(); i++)
	{
		//Fetch music information
		auto SongInfoObj = Exp1::GetMusicInfoFromPage(FolderPath + '\\' + HTMLFileNames[i]);

		//Do word matching
		Exp1::LinkedList<Exp1::WString> WordMatchingResult;
		for (j = 0; j < SongInfoObj.Lyrics.Size(); j++)
			WordMatchingResult += Exp1::DoMatching(Dict2Root, SongInfoObj.Lyrics[j]);

		//Get document ID from its name
		Exp1::String DocIDStr;
		j = 0;
		while ((HTMLFileNames[i][j] >= '0') && (HTMLFileNames[i][j] <= '9'))
		{
			DocIDStr += HTMLFileNames[i][j];
			j++;
		}
		unsigned int DocID = atoi(DocIDStr);
		//Build document dictionary for current document
		BuildDocDictSingle(WordMatchingResult, DocDict, DocID);

		//Store song info
		if (SongsInfo)
			SongsInfo->Put(DocID, SongInfoObj);
	}

	//Return dictionary
	return DocDict;
}

//Do searching using document dictionary
Exp1::Array<Exp1::Array<Exp2::SearchResultItem>> Exp2::DoSearching(Exp1::SBTNode* Dict2Root,
	CurrentHashTable<Exp1::WString, DocDictItem> DocDict,
	Exp1::WString QueryText)
{
	//Query conditions
	Exp1::Array<Exp1::LinkedList<Exp1::WString>> QueryCond;
	//Loop counter
	unsigned int i, j;

	//Parse query text
	auto SplitQueryText = QueryText.Replace(L'\r').Split(L'\n');
	for (i = 0; i < SplitQueryText.Size(); i++)
	{
		//Split single query
		auto SplitSingleQuery = SplitQueryText[i].Split(' ');
		Exp1::LinkedList<Exp1::WString> SingleQueryCond;
		//Do word matching to each part
		for (j = 0; j < SplitSingleQuery.Size(); j++)
			SingleQueryCond += Exp1::DoMatching(Dict2Root, SplitSingleQuery[j]);
		//Append single query condition
		QueryCond.Push(SingleQueryCond);
	}

	//Query result object
	Exp1::Array<Exp1::Array<SearchResultItem>> Result;
	//Begin searching
	for (i = 0; i < QueryCond.Size();i++)
	{
		//One search request each time
		const Exp1::LinkedList<Exp1::WString>& SingleQueryCond = QueryCond[i];
		//Single query condition result
		Exp2::CurrentHashTable<unsigned int, SearchResultItem> TmpSingleResult(Exp1::UIntHash);
		
		//Search process
		auto CondPtr = SingleQueryCond.Begin();
		while (CondPtr != SingleQueryCond.End())
		{
			//Try to find word in the document dictionary
			bool Exists;
			auto DictItem = DocDict.Get(*CondPtr, &Exists);
			//Word not found, ignore word
			if (!Exists)
			{
				CondPtr++;
				continue;
			}

			//Update single query result
			auto DocsListPtr = DictItem.DocsList.Begin();
			while (DocsListPtr != DictItem.DocsList.End())
			{
				SearchResultItem& ResultItem = TmpSingleResult.Data(DocsListPtr->DocID, &Exists);

				//DocID already exists
				if (Exists)
				{
					ResultItem.Count += DocsListPtr->Count;
					ResultItem.OccurWordCount++;
				}
				//DocID not exists, create a new one
				else
					TmpSingleResult.Put(DocsListPtr->DocID, { DocsListPtr->DocID, 1, DocsListPtr->Count });

				DocsListPtr++;
			}

			CondPtr++;
		}

		//Convert result to array
		Exp1::Array<SearchResultItem> SingleResult;
		TmpSingleResult.ForEach([&](unsigned int DocID, SearchResultItem Item)
		{
			SingleResult.Push(Item);
		});
		//Sort the array
		Exp1::Sort(SingleResult, std::function<bool(SearchResultItem, SearchResultItem)>
		([](SearchResultItem RI1, SearchResultItem RI2) -> bool
		{
			//Sort by word occur count
			if (RI1.OccurWordCount != RI2.OccurWordCount)
				return RI1.OccurWordCount < RI2.OccurWordCount;
			//Sort by word total count
			else
				return RI1.Count < RI2.Count;
		}));

		//Add single result to result
		Result.Push(SingleResult);
	}

	//Return result
	return Result;
}

//Serialize result
Exp1::WString Exp2::SerializeSearchResult(Exp1::Array<Exp1::Array<Exp2::SearchResultItem>> SearchResult)
{
	unsigned int i, j;
	Exp1::WString Result;

	for (i = 0; i < SearchResult.Size(); i++)
	{
		//Generate output
		for (j = 0; j < SearchResult[i].Size(); j++)
		{
			Result += L'(';
			Result += Exp1::WString(SearchResult[i][j].DocID);
			Result += L',';
			Result += Exp1::WString(SearchResult[i][j].Count);
			Result += L") ";
		}
		Result += L'\n';
	}
	
	return Result;
}