#include <iostream>
#include <fstream>
#include <locale>
#include "String.h"
#include "ReverseIndexMaker.h"
#include "WordMatching.h"
#include "Hashtable.h"
#include "Util.h"

//Program entry
//Lyric searching
int main(int ArgC, char** ArgV)
{
	//Set locale
	std::locale::global(std::locale(""));
	std::wcout.imbue(std::locale(""));

	//Settings hash table
	Exp1::HashTable<Exp1::String, Exp1::String> Settings;
	//Default settings
	Settings.Put("dict", "../var/exp2_dict.dict3");
	Settings.Put("webpage-dir", "pages_300");
	Settings.Put("query-file", "query1.txt");
	Settings.Put("query-result", "result1.txt");

	//Read dictionary from given address
	Exp1::String Dict2Memory;
	auto DictRoot = Exp1::ReadDict2(Settings["dict"], Dict2Memory);

	//Use cache or not
	bool UseCache = false;
	//Web page directory
	auto WebPageDir = Settings["webpage-dir"];
	//Document directory
	Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem> DocDict, _DocDict;
	//Songs info hash table
	Exp2::CurrentHashTable<unsigned int, Exp1::SongInfo> SongsInfo(Exp1::UIntHash), _SongsInfo(Exp1::UIntHash);

	//Check cache
	std::ifstream CacheIn(WebPageDir + ".ddc", std::ios::binary);
	//Last modified time from cache
	long long CacheLMD;
	//Last modified date
	long long LastModifyDate;

	//Cache file found
	if (CacheIn.good())
		//Read cache content and last modified date
		_DocDict = Exp2::DeserializeDocDict(CacheIn, CacheLMD, &_SongsInfo);
	CacheIn.close();

	Exp1::FStat DirStat;
	//Try to fetch last modified date
	if (Exp1::Stat(WebPageDir, &DirStat) == 0)
	{
		LastModifyDate = DirStat.st_mtime;
		//Folder content not changed, use cache
		if ((CacheIn.good()) && (LastModifyDate == CacheLMD))
		{
			DocDict = _DocDict;
			SongsInfo = _SongsInfo;
			UseCache = true;
		}
	}

	//Cache not used, build document dictionary and cache
	if (!UseCache)
	{
		//Build document dictionary
		DocDict = Exp2::BuildDocDict(DictRoot, WebPageDir, &SongsInfo);
		//Write dictionary content to cache file
		std::ofstream CacheOut(WebPageDir + ".ddc", std::ios::binary);
		if (CacheOut.good())
			Exp2::SerializeDocDict(CacheOut, DocDict, LastModifyDate, &SongsInfo);
		CacheOut.close();
	}

	//Read query content from file
	std::wifstream QueryIn(Settings["query-file"]);
	Exp1::WString QueryContent;
	Exp1::GetAll(QueryIn, QueryContent);
	QueryIn.close();
	//Do searching
	auto SearchResult = Exp2::DoSearching(DictRoot, DocDict, QueryContent);
	//Write result to file
	std::wofstream QueryResultOut(Settings["query-result"]);
	QueryResultOut << Exp2::SerializeSearchResult(SearchResult);
	QueryResultOut.close();

	return 0;
}
