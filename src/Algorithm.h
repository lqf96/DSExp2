#pragma once

#include <functional>
#include "Array.h"

//Experiment I namespace
namespace Exp1
{
	//[ Sort Function ]
	//Merge two array (Used by merge sort)
	template <typename T>
	Array<T> __Merge(Array<T>& C1, Array<T>& C2, std::function<bool(T, T)> Comparer)
	{
		unsigned int I1 = 0, I2 = 0;
		Array<T> Result;

		while ((I1 != C1.Size()) || (I2 != C2.Size()))
		{
			//I1 at end
			if (I1 == C1.Size())
			{
				Result.Push(C2.At(I2));
				I2++;
			}
			//I2 at end
			else if (I2 == C2.Size())
			{
				Result.Push(C1.At(I1));
				I1++;
			}
			//Both not at end, compare
			else if (Comparer(C1.operator[](I1), C2.operator[](I2)))
			{
				Result.Push(C2.At(I2));
				I2++;
			}
			else
			{
				Result.Push(C1.At(I1));
				I1++;
			}
		}

		return Result;
	}

	//Merge sort function
	template <typename T>
	void Sort(Array<T>& _Array, std::function<bool(T, T)> Comparer)
	{
		if (_Array.Size() <= 1)
			return;

		//Split array from middle
		unsigned int Middle = _Array.Size() / 2;
		Array<T> SubArray1 = _Array.SubArray(0, Middle),
			SubArray2 = _Array.SubArray(Middle, _Array.Size());
		//Recursively sort sub-array
		Sort(SubArray1, Comparer);
		Sort(SubArray2, Comparer);
		//Merge array
		_Array = __Merge(SubArray1, SubArray2, Comparer);
	}
}