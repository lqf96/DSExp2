#include "String.h"

//Convert from normal string to wide character string
Exp1::WString Exp1::ToWString(Exp1::String Str)
{
	wchar_t WChar;
	char* WCharPtr = (char*)(&WChar);
	unsigned int i;
	WString Result;

	for (i = 0; i < Str.Size(); i++)
	{
		WCharPtr[i % sizeof(wchar_t)] = Str[i];
		if (i % sizeof(wchar_t) == sizeof(wchar_t) - 1)
			Result.Push(WChar);
	}

	return Result;
}

//Convert from wide character string to normal string
Exp1::String Exp1::ToString(Exp1::WString WStr)
{
	unsigned int i, j;
	String Result;

	for (i = 0; i < WStr.Size(); i++)
	{
		wchar_t WChar = WStr[i];
		char* WCharPtr = (char*)(&WChar);

		for (j = 0; j < sizeof(wchar_t); j++)
			Result += WCharPtr[j];
	}

	return Result;
}