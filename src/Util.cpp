#include "Util.h"
#include <errno.h>

//[ File Utilities ]
//Get the name of the files in a dictionary
Exp1::Array<Exp1::String> Exp1::GetFiles(Exp1::String Folder, Exp1::String FileNameFilter = "*")
{
	Array<String> Result;
	const char* FileName;

#ifdef UNIX
	dirent* DirEntry;
	DIR* Directory = opendir(Folder);

	if (Directory == NULL)
		throw "FailedToFetchFolderInfo";

	while (true)
	{
		DirEntry = readdir(Directory);
		if (!DirEntry)
			break;

		FileName = DirEntry->d_name;
#else
	_finddata_t FolderC;

	Folder += '/' + FileNameFilter;
	auto FileH = _findfirst(Folder, &FolderC);

	if (FileH == -1)
		throw "FailedToFetchFolderInfo";

	Result.Push(FolderC.name);
	while (_findnext(FileH, &FolderC) == 0)
	{
		FileName = FolderC.name;
#endif
		//Ignore this directory and parent directory
		if ((strcmp(".", FileName) == 0) || (strcmp("..", FileName) == 0))
			continue;
		//Add file name to result
		Result.Push(FileName);
	}

	return Result;
}

//[ Hashtable Utilities ]
unsigned int Exp1::UIntHash(unsigned int UInt)
{
	return UInt;
}
