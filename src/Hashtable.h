#pragma once
#include <functional>
#include "LinkedList.h"
#include "String.h"
#include "Util.h"

//This hash table utilizes SBT (Size Balanced Tree) by Chen Qifeng.
//SBT is easy to implement, supports a lot of features, and has high efficiency.
//The paper can be found at "http://zh.scribd.com/doc/3072015/10-%E9%99%88%E5%90%AF%E5%B3%B0-Size-Balanced-Tree".

//Experiment 1 namespace
namespace Exp1
{
	//Hash table class
	template <typename KT, typename VT>
	class HashTable
	{private:
		//[ Hashtable Structures ]
		//Key-value pair
		struct KVPair
		{
			KT Key;
			VT Value;
		};

		//SBT node
		struct SBTNode
		{
			//Hash
			unsigned int Hash;
			//Tree size
			unsigned int Size;
			//Left and right node
			SBTNode* LNode;
			SBTNode* RNode;
			//Data linked list
			LinkedList<KVPair> DataList;
		};

		//[ Member Variables ]
		//Tree root
		SBTNode* Root;
		//Hash function
		std::function<unsigned int(KT)> HashFunc;
		//Reference count
		unsigned int* RefCount;
		//Hash table size
		unsigned int __Size = 0;

		//Empty object (For indicating no existance in hash table)
		static VT EmptyObj;
		
		//[ SBT Operations ]
		//SBT rotation (Left)
		static void LRotate(SBTNode*& Node)
		{
			if (!Node)
				return;
			
			auto K = Node->RNode;
			Node->RNode = K->LNode;
			K->LNode = Node;

			K->Size = Node->Size;
			Node->Size = 1;
			if (Node->LNode)
				Node->Size += Node->LNode->Size;
			if (Node->RNode)
				Node->Size += Node->RNode->Size;

			Node = K;
		}

		//SBT rotation (Right)
		static void RRotate(SBTNode*& Node)
		{
			if (!Node)
				return;

			auto K = Node->LNode;
			Node->LNode = K->RNode;
			K->RNode = Node;

			K->Size = Node->Size;
			Node->Size = 1;
			if (Node->LNode)
				Node->Size += Node->LNode->Size;
			if (Node->RNode)
				Node->Size += Node->RNode->Size;

			Node = K;
		}

		//SBT maintainance
		static void Maintain(SBTNode*& Node, bool Flag)
		{
			if (Flag)
			{
				unsigned int RRSize = (Node && (Node->RNode) && (Node->RNode->RNode)) ? Node->RNode->RNode->Size : 0;
				unsigned int RLSize = (Node && (Node->RNode) && (Node->RNode->LNode)) ? Node->RNode->LNode->Size : 0;
				unsigned int LSize = (Node && (Node->LNode)) ? Node->LNode->Size : 0;

				if (RRSize > LSize)
					LRotate(Node);
				else if (RLSize > LSize)
				{
					RRotate(Node->RNode);
					LRotate(Node);
				}
				else
					return;
			}
			else
			{
				unsigned int LLSize = (Node && (Node->LNode) && (Node->LNode->LNode)) ? Node->LNode->LNode->Size : 0;
				unsigned int LRSize = (Node && (Node->LNode) && (Node->LNode->RNode)) ? Node->LNode->RNode->Size : 0;
				unsigned int RSize = (Node && (Node->RNode)) ? Node->RNode->Size : 0;

				if (LLSize > RSize)
					RRotate(Node);
				else if (LRSize > RSize)
				{
					LRotate(Node->LNode);
					RRotate(Node);
				}
				else
					return;
			}

			Maintain(Node->LNode, false);
			Maintain(Node->RNode, true);
			Maintain(Node, false);
			Maintain(Node, true);
		}

		//SBT insertion
		static void Insert(SBTNode*& Node, KVPair Pair, unsigned int Hash)
		{
			if (Node == nullptr)
			{
				Node = new SBTNode;
				Node->LNode = Node->RNode = nullptr;
				Node->Hash = Hash;
				Node->Size = 1;
				Node->DataList.Push(Pair);
			}
			else
			{
				Node->Size++;
				if (Hash < Node->Hash)
					Insert(Node->LNode, Pair, Hash);
				else
					Insert(Node->RNode, Pair, Hash);
				Maintain(Node, Hash >= Node->Hash);
			}
		}

		//SBT searching
		static SBTNode* Search(SBTNode* Node, unsigned int Hash)
		{
			if (Node == nullptr)
				return nullptr;
			if (Node->Hash == Hash)
				return Node;

			SBTNode* Result = nullptr;
			if (Hash < Node->Hash)
				Result = Search(Node->LNode, Hash);
			else
				Result = Search(Node->RNode, Hash);
			return Result;
		}

		//SBT deletion (Experimental)
		static unsigned int Delete(SBTNode*& Node, unsigned int Hash)
		{
			unsigned int DeleteHash;

			Node->Size--;
			if ((Hash == Node->Hash) || ((Hash < Node->Hash) && (!Node->LNode)) || ((Hash > Node->Hash) && (!Node->RNode)))
			{
				DeleteHash = Node->Hash;
				if (!Node->LNode)
					Node = Node->RNode;
				else if (!Node->RNode)
					Node = Node->LNode;
				else
					Node->Hash = Delete(Node->LNode, Node->Hash + 1);
			}
			else
			{
				if (Hash < Node->Hash)
					DeleteHash = Delete(Node->LNode, Hash);
				else
					DeleteHash = Delete(Node->RNode, Hash);
			}

			return DeleteHash;
		}

		//Tree cloning
		static SBTNode* CloneTree(SBTNode* Node)
		{
			if (Node == NULL)
				return NULL;
			
			SBTNode* NewNode = new SBTNode(*Node);
			NewNode->LNode = CloneTree(Node->LNode);
			NewNode->RNode = CloneTree(Node->RNode);

			return NewNode;
		}

		//Tree Destroying
		static void DeleteTree(SBTNode* Node)
		{
			if (Node == NULL)
				return;

			DeleteTree(Node->LNode);
			DeleteTree(Node->RNode);
			delete Node;
		}

		//Tree traversal helper (Preorder traversal)
		static void TraverseIA(SBTNode* Node, std::function<void(KT, VT)> Func)
		{
			if (!Node)
				return;

			TraverseIA(Node->LNode, Func);
			TraverseII(Node, Func);
			TraverseIA(Node->RNode, Func);
		}

		//Tree traversal helper (Traverse in key-value list)
		static void TraverseII(SBTNode* Node, std::function<void(KT, VT)> Func)
		{
			auto Ptr = Node->DataList.Begin();
			while (Ptr != Node->DataList.End())
			{
				Func(Ptr->Key, Ptr->Value);
				Ptr++;
			}
		}

		//[ Member Methods ]
		//Clone internal tree
		void Clone()
		{
			//New internal tree
			this->Root = CloneTree(this->Root);

			//Update reference count
			(*this->RefCount)--;
			//Set new reference count
			this->RefCount = new unsigned int(1);
		}
	public:
		//[ Member Methods ]
		//Constructor
		HashTable(std::function<unsigned int(KT)> _HashFunc)
		{
			this->HashFunc = _HashFunc;
			this->Root = nullptr;
			this->RefCount = new unsigned int(1);
			this->__Size = 0;
		}

		//Simple constructor (Use KT::Hash as hash function)
		HashTable() : HashTable(Exp1::WrapMemberFunc(&KT::Hash)) {}

		//Copy constructor
		HashTable(const HashTable<KT, VT>& Another)
		{
			this->Root = nullptr;
			this->RefCount = nullptr;

			//Use "=" operator when copying
			*this = Another;
		}

		//Destructor
		~HashTable()
		{
			(*this->RefCount)--;
			//No reference pointing to internal tree
			if (*this->RefCount == 0)
			{
				DeleteTree(this->Root);
				delete this->RefCount;
			}
		}

		//Overload operator "=" for array
		HashTable<KT, VT>& operator =(const HashTable<KT, VT>& Another)
		{
			//Copy size
			this->__Size = Another.__Size;
			//Copy hash function
			this->HashFunc = Another.HashFunc;

			//Internal tree not same
			if (this->RefCount != Another.RefCount)
			{
				//Only one count to internal tree, destroy internal tree and reference count
				if ((this->RefCount) && (*this->RefCount == 1))
				{
					DeleteTree(this->Root);
					delete this->RefCount;
				}

				//Copy internal tree and reference count
				this->Root = Another.Root;
				this->RefCount = Another.RefCount;
				//Update reference count
				(*this->RefCount)++;
			}

			return *this;
		}

		//Put key-value pair
		void Put(KT Key, VT Value)
		{
			unsigned int Hash = this->HashFunc(Key);
			//Clone the tree when necessary
			if (*this->RefCount > 1)
				this->Clone();

			//Try searching for node with same hash value
			auto HashNode = Search(this->Root, Hash);
			//Node found
			if (HashNode)
			{
				//Search in the data list
				auto Ptr = HashNode->DataList.Begin();
				while (Ptr != HashNode->DataList.End())
				{
					//Key found, modify its value
					if (Ptr->Key == Key)
					{
						Ptr.Data().Value = Value;
						return;
					}
					Ptr++;
				}

				//Key not found, create a new key-value pair
				KVPair DataPair;
				DataPair.Key = Key;
				DataPair.Value = Value;
				HashNode->DataList.Push(DataPair);
				this->__Size++;
			}
			//Not found
			else
			{
				KVPair DataPair;
				DataPair.Key = Key;
				DataPair.Value = Value;
				Insert(this->Root, DataPair, Hash);
				this->__Size++;
			}
		}

		//Get value from key (Read-only access)
		const VT& Get(KT Key, bool* Exists = nullptr) const
		{
			unsigned int Hash = this->HashFunc(Key);

			//Search for the node with the same hash value
			auto HashNode = Search(this->Root, Hash);
			//Node found
			if (HashNode)
			{
				auto Ptr = HashNode->DataList.Begin();

				//Search for the key in the node data list
				while (Ptr != HashNode->DataList.End())
				{
					if (Ptr->Key == Key)
					{
						if (Exists)
							*Exists = true;
						return Ptr->Value;
					}
					Ptr++;
				}
			}

			//Key not found, return default value
			if (Exists)
				*Exists = false;
			return HashTable<KT, VT>::EmptyObj;
		}

		//Remove element (Not implemented)

		//Check if the hashtable contains a specific key
		bool Contains(KT Key) const
		{
			bool __Contains;
			this->Get(Key, &__Contains);
			return __Contains;
		}

		//Check if the hashtable contains a specific value
		bool ContainsValue(VT Value, KT* Key = nullptr)
		{
			//Traverse in the hash table to find given value
			bool Exists = false;
			this->ForEach([&](KT TKey, VT TValue)
			{
				if (Exists)
					return;
				if (TValue == Value)
				{
					Exists = true;
					if (Key)
						*Key = TKey;
				}
			});

			return Exists;
		}

		//Override operator "[]" for read-only access
		const VT& operator [](KT Key) const
		{
			return this->Get(Key);
		}

		//Read write access to hash table
		VT& Data(KT Key, bool* Exists = nullptr)
		{
			if (*this->RefCount > 1)
				this->Clone();
			return const_cast<VT&>(this->Get(Key, Exists));
		}

		//Get size of the hash table
		unsigned int Size() const
		{
			return this->__Size;
		}

		//Check if the hash table is empty
		bool Empty() const
		{
			return this->__Size == 0;
		}

		//Traverse through the hash table (Preorder traversal)
		void ForEach(std::function<void(KT, VT)> Func) const
		{
			TraverseIA(this->Root, Func);
		}
	};

	//Empty object (For indicating no existance in hash table)
	template <typename KT, typename VT>
	VT HashTable<KT, VT>::EmptyObj;
}