#pragma once

#include <functional>
#include <utility>
#include <tuple>
#include <iostream>
#include "Array.h"
#include "MusicInfoFetcher.h"
#include "ReverseIndexMaker.h"
#include "Util.h"

//Experiment 2 namespace
namespace Exp2
{
	//Recommendation weight function list
	extern Exp1::Array<std::function<double(Exp1::SongInfo, Exp1::SongInfo)>> RecommendWeightFuncList;
	//Declare weight functions
	DECLARE_IMMEDIATE_CODE(WeightFuncInitialize)

	//Do recommendation
	Exp1::Array<Exp1::Array<std::pair<unsigned int, Exp1::WString>>> DoRecommendation(
		CurrentHashTable<unsigned int, Exp1::SongInfo>& SongsInfo,
		Exp1::WString QueryString);
	//Serialize recommendation result
	void SerializeRecommendationResult(std::basic_ostream<wchar_t>& Out,
		Exp1::Array<Exp1::Array<std::pair<unsigned int, Exp1::WString>>>& Result);
}