#include "HTMLParser.h"

//Private constructor
Exp1::HTMLElement::HTMLElement(HTMLParser* _Doc, HTMLParser::HTMLNode* _Node)
{
	this->Document = _Doc;
	this->Node = _Node;
}

//Constructor
Exp1::HTMLElement::HTMLElement()
{
	this->Document = nullptr;
	this->Node = nullptr;
}

//Get inner text
Exp1::WString Exp1::HTMLElement::InnerText() const
{
	HTMLParser::HTMLNode* CurrentNode = this->Node->LNode;
	WString Result;

	while (CurrentNode)
	{
		//Text node
		if (CurrentNode->Type == HTMLParser::HTMLNode::Text)
			Result += CurrentNode->Value;
		//Element node
		else if (CurrentNode->Type == HTMLParser::HTMLNode::Element)
			Result += HTMLElement(this->Document, CurrentNode).InnerText();
		CurrentNode = CurrentNode->RNode;
	}

	return Result;
}

//Get child elements
Exp1::Array<Exp1::HTMLElement> Exp1::HTMLElement::ChildElements() const
{
	HTMLParser::HTMLNode* CurrentChild = this->Node->LNode;
	Array<HTMLElement> Result;

	while (CurrentChild)
	{
		if (CurrentChild->Type == HTMLParser::HTMLNode::Element)
			Result.Push(HTMLElement(this->Document, CurrentChild));
		CurrentChild = CurrentChild->RNode;
	}

	return Result;
}

Exp1::WString Exp1::HTMLElement::Attribute(Exp1::WString Name, bool* Found) const
{
	HTMLParser::HTMLNode* CurrentNode = this->Node->LNode;
	if (CurrentNode)
		while (CurrentNode->Type == HTMLParser::HTMLNode::Prop)
		{
			if (CurrentNode->Value == Name)
			{
				if (Found)
					*Found = true;
				return CurrentNode->LNode->Value;
			}
			CurrentNode = CurrentNode->RNode;
		}
	if (Found)
		*Found = false;
	return L"";
}