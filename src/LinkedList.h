#pragma once

#include <functional>

//Experiment 1 namespace
namespace Exp1
{
	//Linked list class
	template <typename T>
	class LinkedList
	{private:
		//Linked list item class
		class LinkedListItem
		{public:
			//Data
			T Data;
			//Previous and next item
			LinkedListItem* Prev;
			LinkedListItem* Next;

			//Constructor
			LinkedListItem(T _Data = T()) : Data(_Data), Prev(nullptr), Next(nullptr) {}
		};

		//Begin and end node
		LinkedListItem* __Begin;
		LinkedListItem* __End;
		//Linked list size
		unsigned int __Size;
		//Reference count
		unsigned int* RefCount;

		//Clone internal linked list
		void Clone()
		{
			LinkedListItem* NewBegin;
			LinkedListItem* NewEnd;
			LinkedListItem* __Iterator = this->__Begin->Next;
			LinkedListItem* __NewIterator;
			unsigned int* NewRefCount = new unsigned int(1);

			NewBegin = new LinkedListItem();
			__NewIterator = NewBegin;
			while (__Iterator->Next != nullptr)
			{
				LinkedListItem* CurrentNewItem = new LinkedListItem(__Iterator->Data);
				CurrentNewItem->Prev = __NewIterator;
				__NewIterator->Next = CurrentNewItem;

				__NewIterator = CurrentNewItem;
				__Iterator = __Iterator->Next;
			}
			NewEnd = new LinkedListItem();
			NewEnd->Prev = __NewIterator;
			__NewIterator->Next = NewEnd;

			(*this->RefCount)--;

			this->__Begin = NewBegin;
			this->__End = NewEnd;
			this->RefCount = NewRefCount;
		}
	public:
		//Linked list iterator
		class Iterator
		{private:
			//Current item
			LinkedListItem* CurrentItem;
			//List reference
			LinkedList<T>* __List;

			//Private constructor
			Iterator(LinkedListItem* Item, const LinkedList<T>* _List)
			{
				this->CurrentItem = Item;
				this->__List = const_cast<LinkedList<T>*>(_List);
			}

			//Friend class
			friend class LinkedList<T>;
		public:
			//Get linked list
			LinkedList* Container() const
			{
				return this->__List;
			}

			//Get data
			const T& operator *() const
			{
				return this->CurrentItem->Data;
			}

			//Get data
			const T* operator ->() const
			{
				return &(this->CurrentItem->Data);
			}

			//Editable data reference
			T& Data()
			{
				if (*(this->__List->RefCount) > 1)
					this->__List->Clone();
				return const_cast<T&>(this->operator*());
			}

			//Move to next item
			Iterator operator ++()
			{
				if (this->CurrentItem->Next == nullptr)
					throw "EndOfLinkedList";

				this->CurrentItem = this->CurrentItem->Next;
				return *this;
			}

			//Move to next item
			Iterator operator ++(int)
			{
				if (this->CurrentItem->Next == nullptr)
					throw "EndOfLinkedList";

				Iterator NewIterator(*this);
				this->CurrentItem = this->CurrentItem->Next;
				return NewIterator;
			}

			//Move to previous item
			Iterator operator --()
			{
				if (this->CurrentItem->Prev == nullptr)
					throw "BeginOfLinkedList";

				this->CurrentItem = this->CurrentItem->Prev;
				return *this;
			}

			//Move to previous item
			Iterator operator --(int)
			{
				if (this->CurrentItem->Prev == nullptr)
					throw "BeginOfLinkedList";

				Iterator NewIterator(*this);
				this->CurrentItem = this->CurrentItem->Prev;
				return NewIterator;
			}

			//Compare item
			bool operator ==(Iterator Ptr) const
			{
				return (this->CurrentItem == Ptr.CurrentItem) && (this->__List == Ptr.__List);
			}

			//Compare item
			bool operator !=(Iterator Ptr) const
			{
				return (this->CurrentItem != Ptr.CurrentItem) || (this->__List != Ptr.__List);
			}
		};

		//Constructor
		LinkedList()
		{
			this->__Begin = new LinkedListItem();
			this->__End = new LinkedListItem();
			this->__Begin->Next = this->__End;
			this->__End->Prev = this->__Begin;
			
			this->__Size = 0;
			this->RefCount = new unsigned int(1);
		}

		//Copy constructor
		LinkedList(const LinkedList<T>& _List)
		{
			this->__Begin = nullptr;
			this->__End = nullptr;
			this->__Size = 0;
			this->RefCount = nullptr;

			//Call operator "="
			*this = _List;
		}

		//Destructor
		~LinkedList()
		{
			(*this->RefCount)--;
			if (*this->RefCount == 0)
			{
				LinkedListItem* __CurrentItem = this->__Begin;
				while (__CurrentItem != nullptr)
				{
					LinkedListItem* __Tmp = __CurrentItem->Next;
					delete __CurrentItem;
					__CurrentItem = __Tmp;
				}
				delete this->RefCount;
			}
		}

		//Return linked list size
		unsigned int Size() const
		{
			return this->__Size;
		}

		//Check if the linked list is empty
		bool Empty() const
		{
			return this->__Size == 0;
		}

		//Get first item (Read-only)
		const T& First() const
		{
			if (this->__Size != 0)
				return this->__Begin->Next->Data;
			else
				throw "EmptyArray";
		}

		//Get last item (Read-only)
		const T& Last() const
		{
			if (this->__Size != 0)
				return this->__End->Prev->Data;
			else
				throw "EmptyArray";
		}

		//Get begin iterator
		Iterator Begin() const
		{
			return Iterator(this->__Begin->Next, this);
		}
		
		//Get end iterator
		Iterator End() const
		{
			return Iterator(this->__End, this);
		}
		
		//Get reverse begin iterator
		Iterator RBegin() const
		{
			return Iterator(this->__End->Prev, this);
		}

		//Get reverse end iterator
		Iterator REnd() const
		{
			return Iterator(this->__Begin, this);
		}

		//Push an item to the end of the array
		void Push(T Item)
		{
			this->Add(Item, Iterator(this->__End->Prev, this));
		}

		//Overload operator "+" for a single item
		LinkedList<T> operator +(T Item) const
		{
			LinkedList<T>& NewList = *this;
			NewList.Push(Item);
			return NewList;
		}

		//Overload operator "+=" for a single item
		LinkedList<T>& operator +=(T Item)
		{
			this->Push(Item);
			return this;
		}

		//Overload operator "+" for an array
		LinkedList<T> operator +(const LinkedList<T>& _Array) const
		{
			LinkedList<T> NewList = *this;
			LinkedListItem* __CurrentItem = _Array.__Begin->Next;

			while (__CurrentItem->Next != nullptr)
			{
				NewList.Push(__CurrentItem->Data);
				__CurrentItem = __CurrentItem->Next;
			}
			return NewList;
		}

		//Overload operator "+=" for an array
		LinkedList<T>& operator +=(const LinkedList<T>& _Array)
		{
			LinkedListItem* __CurrentItem = _Array.__Begin->Next;

			while (__CurrentItem->Next != nullptr)
			{
				this->Push(__CurrentItem->Data);
				__CurrentItem = __CurrentItem->Next;
			}
			return *this;
		}

		//Overload operator "=" for array
		LinkedList<T>& operator =(const LinkedList<T>& _List)
		{
			this->__Size = _List.__Size;

			if (this->RefCount != _List.RefCount)
			{
				if ((this->RefCount) && (*this->RefCount == 1))
					this->~LinkedList();

				this->__Begin = _List.__Begin;
				this->__End = _List.__End;
				this->RefCount = _List.RefCount;
				(*this->RefCount)++;
			}

			return *this;
		}

		//Add an item to the array
		void Add(T Item, Iterator __Iterator, bool After = true)
		{
			if (__Iterator.__List != this)
				throw "IteratorNotBelongToLinkedList";
			if (((__Iterator == this->End()) && After) || ((__Iterator == this->REnd()) && !After))
				throw "IteratorOutOfRange";

			if (*this->RefCount > 1)
				this->Clone();

			LinkedListItem* NewItem = new LinkedListItem(Item);
			//Insert before or after the node
			if (!After)
				__Iterator--;
			//Do insertion
			NewItem->Next = __Iterator.CurrentItem->Next;
			NewItem->Prev = __Iterator.CurrentItem;
			__Iterator.CurrentItem->Next->Prev = NewItem;
			__Iterator.CurrentItem->Next = NewItem;

			//Update size
			this->__Size++;
		}

		//Pop an item from the end of the array
		T Pop()
		{
			return this->Remove(this->__End->Prev);
		}

		//Remove an item from the array
		T Remove(Iterator& __Iterator)
		{
			if (__Iterator->List != this)
				throw "IteratorNotBelongToLinkedList";
			if ((__Iterator == this->__Begin) || (__Iterator == this->__End))
				throw "IteratorOutOfRange";

			if (*this->RefCount > 1)
				this->Clone();

			//Fetch removed item
			T RemovedItem = __Iterator->CurrentItem->Data;
			//Remove item
			LinkedListItem* NewPosition = __Iterator->CurrentItem->Next;
			__Iterator->CurrentItem->Prev->Next = NewPosition;
			NewPosition->Prev = __Iterator->CurrentItem->Prev;
			delete __Iterator->CurrentItem;
			__Iterator->CurrentItem = NewPosition;

			//Update size
			this->__Size--;
			return RemovedItem;
		}

		//Check if the array contains an item
		bool Contains(T Item, Iterator* _Iterator = nullptr) const
		{
			Iterator __Iterator = this->Begin();
			
			while (__Iterator != this->End())
			{
				if (__Iterator->Data == Item)
				{
					if (_Iterator)
						*_Iterator = __Iterator;
					return true;
				}
				__Iterator++;
			}
			return false;
		}

		//Iterate through the linked list
		void ForEach(std::function<void(T)> Func) const
		{
			Iterator Ptr = this->Begin();
			while (Ptr != this->End())
			{
				Func(*Ptr);
				Ptr++;
			}
		}

		//Hash function
		unsigned int Hash()
		{
			unsigned int Seed = 131;
			unsigned int _Hash = 0;
			unsigned int i;
			auto Ptr = this->Begin();

			while (Ptr != this->End())
			{
				const unsigned char* _Ptr = (const unsigned char*)(&(*Ptr));
				for (i = 0; i < sizeof(T); i++)
					_Hash = _Hash * Seed + _Ptr[i];
				Ptr++;
			}

			return _Hash;
		}
	};

	//Use linked list as stack
	template <typename T>
	using LStack = LinkedList<T>;
}