#include "ReverseIndexMaker.h"

//Serialize document dictionary (as cache)
void Exp2::SerializeDocDict(std::basic_ostream<char>& Out,
	Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem>& DocDict,
	long long LastModifyDate,
	Exp2::CurrentHashTable<unsigned int, Exp1::SongInfo>* SongsInfo)
{
	//Document dictionary cache magic
	Out << "DDC1";
	//Last modify date
	Exp1::WriteData(Out, LastModifyDate);
	
	//Dictionary size
	Exp1::WriteData(Out, DocDict.Size());
	//Serialize document dictionary
	DocDict.ForEach([&](Exp1::WString Word, Exp2::DocDictItem Item)
	{
		//Word length (in bytes)
		Exp1::WriteData(Out, Word.Size() * sizeof(wchar_t));
		//Word
		Out << Exp1::ToString(Word);
		//Word ID
		Exp1::WriteData(Out, Item.WordID);
		//Occur count
		Exp1::WriteData(Out, Item.OccurCount);
		//Occur in file content
		Exp1::WriteData(Out, Item.OccurInFileCount);
		
		//Document list length
		Exp1::WriteData(Out, Item.DocsList.Size());
		//Write document list
		auto Ptr = Item.DocsList.Begin();
		while (Ptr != Item.DocsList.End())
		{
			//Document ID
			Exp1::WriteData(Out, Ptr->DocID);
			//Word count
			Exp1::WriteData(Out, Ptr->Count);
			Ptr++;
		}
	});

	if (SongsInfo)
	{
		//Songs info size
		Exp1::WriteData(Out, SongsInfo->Size());
		//Serialize each song information object
		SongsInfo->ForEach([&](unsigned int DocID, Exp1::SongInfo Info)
		{
			//Document ID
			Exp1::WriteData(Out, DocID);
			//Title (And length in bytes)
			Exp1::WriteData(Out, Info.Title.Size() * sizeof(wchar_t));
			Out << Exp1::ToString(Info.Title);
			//Singer (And length in bytes)
			Exp1::WriteData(Out, Info.Singer.Size() * sizeof(wchar_t));
			Out << Exp1::ToString(Info.Singer);
			//Album (And length in bytes)
			Exp1::WriteData(Out, Info.Album.Size() * sizeof(wchar_t));
			Out << Exp1::ToString(Info.Album);
			//Composer (And length in bytes)
			Exp1::WriteData(Out, Info.Composer.Size() * sizeof(wchar_t));
			Out << Exp1::ToString(Info.Composer);
			//Lyrics maker (And length in bytes)
			Exp1::WriteData(Out, Info.LyricsMaker.Size() * sizeof(wchar_t));
			Out << Exp1::ToString(Info.LyricsMaker);
			//Release time
			Exp1::WriteData(Out, Info.ReleaseTime.Size() * sizeof(wchar_t));
			Out << Exp1::ToString(Info.ReleaseTime);
			//Lyrics
			Exp1::WriteData(Out, Info.Lyrics.Size());
			Info.Lyrics.ForEach([&](Exp1::WString SingleLyric)
			{
				Exp1::WriteData(Out, SingleLyric.Size() * sizeof(wchar_t));
				Out << Exp1::ToString(SingleLyric);
			});
		});
	}
}

//Deserialize document dictionary
Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem> Exp2::DeserializeDocDict(std::basic_istream<char>& In,
	long long& LastModifyDate,
	Exp2::CurrentHashTable<unsigned int, Exp1::SongInfo>* SongsInfo)
{
	unsigned int i, j;

	//Magic string
	Exp1::String Magic = "DDC1";
	char ActualMagic[5];
	//Check magic string
	In.read(ActualMagic, 4);
	ActualMagic[4] = '\0';
	if (Magic != ActualMagic)
		throw "IllegalCacheFile";

	//Result
	Exp2::CurrentHashTable<Exp1::WString, Exp2::DocDictItem> DocDict;

	//Read last modify date
	LastModifyDate = Exp1::ReadData<long long>(In);
	//Read dictionary size
	unsigned int DocDictSize = Exp1::ReadData<unsigned int>(In);
	//Read dictionary content
	for (i = 0; i < DocDictSize; i++)
	{
		//Document dictionary item
		DocDictItem Item;
		
		//Read word length (In bytes) and word
		size_t WordSize = Exp1::ReadData<size_t>(In);
		Item.Word = Exp1::ToWString(Exp1::ReadString(In, WordSize));
		//Word ID
		Item.WordID = Exp1::ReadData<unsigned int>(In);
		//Occur count
		Item.OccurCount = Exp1::ReadData<unsigned int>(In);
		//Occur in file content
		Item.OccurInFileCount = Exp1::ReadData<unsigned int>(In);

		//Document list length
		auto DocListSize = Exp1::ReadData<unsigned int>(In);
		//Write document list
		for (j = 0; j < DocListSize; j++)
		{
			WordSingleDocItem WSDItem;
			//Document ID
			WSDItem.DocID = Exp1::ReadData<unsigned int>(In);
			//Word count
			WSDItem.Count = Exp1::ReadData<unsigned int>(In);
			Item.DocsList.Push(WSDItem);
		}

		DocDict.Put(Item.Word, Item);
	}

	if (SongsInfo)
	{
		//Songs info size
		unsigned int SongsInfoSize = Exp1::ReadData<unsigned int>(In);
		//Deserialize each song information object
		for (i = 0; i < SongsInfoSize;i++)
		{
			size_t TmpSize;
			Exp1::SongInfo InfoObj;

			//Document ID
			unsigned int DocID = Exp1::ReadData<unsigned int>(In);
			//Title (And length in bytes)
			TmpSize = Exp1::ReadData<size_t>(In);
			InfoObj.Title = Exp1::ToWString(Exp1::ReadString(In, TmpSize));
			//Singer (And length in bytes)
			TmpSize = Exp1::ReadData<size_t>(In);
			InfoObj.Singer = Exp1::ToWString(Exp1::ReadString(In, TmpSize));
			//Album (And length in bytes)
			TmpSize = Exp1::ReadData<size_t>(In);
			InfoObj.Album = Exp1::ToWString(Exp1::ReadString(In, TmpSize));
			//Composer (And length in bytes)
			TmpSize = Exp1::ReadData<size_t>(In);
			InfoObj.Composer = Exp1::ToWString(Exp1::ReadString(In, TmpSize));
			//Lyrics maker (And length in bytes)
			TmpSize = Exp1::ReadData<size_t>(In);
			InfoObj.LyricsMaker = Exp1::ToWString(Exp1::ReadString(In, TmpSize));
			//Release time
			TmpSize = Exp1::ReadData<size_t>(In);
			InfoObj.ReleaseTime = Exp1::ToWString(Exp1::ReadString(In, TmpSize));
			//Lyrics
			TmpSize = Exp1::ReadData<unsigned int>(In);
			for (j = 0; j < TmpSize;j++)
			{
				size_t SingleLyricSize = Exp1::ReadData<size_t>(In);
				InfoObj.Lyrics.Push(Exp1::ToWString(Exp1::ReadString(In, SingleLyricSize)));
			}

			SongsInfo->Put(DocID, InfoObj);
		}
	}

	//Return dictionary content
	return DocDict;
}