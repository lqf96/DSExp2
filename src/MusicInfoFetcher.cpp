#include "MusicInfoFetcher.h"

//Get music information from page
Exp1::SongInfo Exp1::GetMusicInfoFromPage(Exp1::String Path)
{
	//Parse HTML document and fetch related elements
	Exp1::HTMLParser Parser(Path);
	auto SongInfoElements = Parser.GetElementsByClassName(L"song_detail")[0].ChildElements();
	Exp1::SongInfo Result;
	unsigned int i;

	//Fetch information
	Result.Title = Parser.GetElementsByClassName(L"song_tit")[0].ChildElements()[0].Attribute(L"title");
	Result.Singer = SongInfoElements[0].ChildElements()[0].InnerText();
	Result.Album = SongInfoElements[2].ChildElements()[0].InnerText();
	Result.ReleaseTime = SongInfoElements[3].ChildElements()[0].InnerText();
	auto RawLyrics = Parser.GetElementsByTagName(L"textarea")[0].InnerText();

	//Try to get more informations from array
	Result.Composer = L"[������]";
	Result.LyricsMaker = L"[������]";
	auto RawLyricsArray = RawLyrics.Split(L'\n');
	Exp1::Array<Exp1::WString> LyricsArray;
	bool MetaParsing = true;
	for (i = 1; i < RawLyricsArray.Size(); i++)
		//Meta data parsing
		if (MetaParsing)
		{
			//Composer
			if ((RawLyricsArray[i].Size() > 2) && ((RawLyricsArray[i].SubStr(0, 2) == L"����") || (RawLyricsArray[i].SubStr(0, 2) == L"��:")))
				Result.Composer = RawLyricsArray[i].SubStr(2);
			//Lyrics maker
			else if ((RawLyricsArray[i].Size() > 2) && ((RawLyricsArray[i].SubStr(0, 2) == L"�ʣ�") || (RawLyricsArray[i].SubStr(0, 2) == L"��:")))
				Result.LyricsMaker = RawLyricsArray[i].SubStr(2);
			//End meta data parsing
			else
			{
				MetaParsing = false;
				Result.Lyrics.Push(RawLyricsArray[i]);
			}
		}
		//Lyrics
		else
			Result.Lyrics.Push(RawLyricsArray[i]);

	return Result;
}
