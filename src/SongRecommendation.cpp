#include "SongRecommendation.h"

//Do recommendation
Exp1::Array<Exp1::Array<std::pair<unsigned int, Exp1::WString>>> Exp2::DoRecommendation(
	CurrentHashTable<unsigned int, Exp1::SongInfo>& SongsInfo,
	Exp1::WString QueryString)
{
	//Split query string by '\n'
	auto SplitQueryString = QueryString.Replace('\r').Split('\n');
	//Query result
	Exp1::Array<Exp1::Array<std::pair<unsigned int, Exp1::WString>>> Result;

	//Process query
	SplitQueryString.ForEach([&](Exp1::WString SongName)
	{
		//Check if song info exists
		bool Exists = false;
		Exp1::SongInfo Song;
		SongsInfo.ForEach([&](unsigned int, Exp1::SongInfo _Song)
		{
			if (Exists)
				return;
			if (_Song.Title == SongName)
			{
				Exists = true;
				Song = _Song;
			}
		});

		//Not exists
		if (!Exists)
		{
			Result.Push(Exp1::Array<std::pair<unsigned int, Exp1::WString>>());
			return;
		}

		//Song exists, calculate recommendation weight
		Exp1::Array<std::pair<unsigned int, Exp1::WString>> SingleResult;
		Exp1::Array<std::tuple<unsigned int, Exp1::WString, double>> RecommendWeight;
		SongsInfo.ForEach([&](unsigned int DocID, Exp1::SongInfo Other)
		{
			//Ignore self
			if ((Other.Title == Song.Title) && (Other.Singer == Song.Singer))
				return;

			//Calculate weight
			double Weight = 0;
			RecommendWeightFuncList.ForEach([&](std::function<double(Exp1::SongInfo, Exp1::SongInfo)> WeightFunc)
			{
				Weight += WeightFunc(Song, Other);
			});

			//Add to recommend weight array
			RecommendWeight.Push(std::make_tuple(DocID, Other.Title, Weight));
		});

		//Sort by recommend weight
		Exp1::Sort(RecommendWeight, std::function<bool(std::tuple<unsigned int, Exp1::WString, double>, std::tuple<unsigned int, Exp1::WString, double>)>
		([](std::tuple<unsigned int, Exp1::WString, double> Item1, std::tuple<unsigned int, Exp1::WString, double> Item2)
		{
			return std::get<2>(Item1) < std::get<2>(Item2);
		}));

		//Fetch 10 songs as result
		unsigned int i;
		for (i = 0; i < 10; i++)
			SingleResult.Push(std::pair<unsigned int, Exp1::WString>(std::get<0>(RecommendWeight[i]), std::get<1>(RecommendWeight[i])));
		//Save single result
		Result.Push(SingleResult);
	});

	//Return result
	return Result;
}

//Serialize recommendation result
void Exp2::SerializeRecommendationResult(std::basic_ostream<wchar_t>& Out,
	Exp1::Array<Exp1::Array<std::pair<unsigned int, Exp1::WString>>>& Result)
{
	unsigned int i, j;

	for (i = 0; i < Result.Size(); i++)
	{
		//Song not found
		if (Result[i].Empty())
			Out << L"[δ�ҵ���Ӧ����]";
		//Song found
		else
		{
			for (j = 0; j < 10; j++)
			{
				Out << '(' << Result[i][j].first << ',' << Result[i][j].second << ')';
				if (j != 9)
					Out << ',';
			}
		}

		Out << std::endl;
	}
}

namespace Exp2
{
	//Recommendation weight function list
	Exp1::Array<std::function<double(Exp1::SongInfo, Exp1::SongInfo)>> RecommendWeightFuncList;

	//Declare weight functions
	BEGIN_IMMEDIATE_CODE(WeightFuncInitialize)
		//Album name
		RecommendWeightFuncList.Push([](Exp1::SongInfo Song, Exp1::SongInfo Other) -> double
		{
			return (Song.Album == Other.Album) ? 1 : 0;
		});
		//Singer
		RecommendWeightFuncList.Push([](Exp1::SongInfo Song, Exp1::SongInfo Other) -> double
		{
			return (Song.Singer == Other.Singer) ? 0.8 : 0;
		});
		//Lyrics maker
		RecommendWeightFuncList.Push([](Exp1::SongInfo Song, Exp1::SongInfo Other) -> double
		{
			return (Song.LyricsMaker == Other.LyricsMaker) ? 0.4 : 0;
		});
		//Composer
		RecommendWeightFuncList.Push([](Exp1::SongInfo Song, Exp1::SongInfo Other) -> double
		{
			return (Song.Composer == Other.Composer) ? 0.2 : 0;
		});
	END_IMMEDIATE_CODE
}