#include "HTMLParser.h"

//HTML parser class
//Constructor
Exp1::HTMLParser::HTMLParser(Exp1::String Path)
{
	//Load file and read HTML into a string
	std::wifstream HTMLFileIn(Path, std::ios::in);
	Exp1::WString HTMLContent;
	if (!HTMLFileIn.good())
		throw "FailedToOpenFile";
	Exp1::GetAll(HTMLFileIn, HTMLContent);
	HTMLFileIn.close();

	//Preparation
	unsigned int i = 0;
	this->RootNode = new HTMLNode(HTMLNode::__Internal);
	//Begin parsing
	HTMLParser::HTMLParserCore(i, HTMLContent, this->RootNode);
}

//HTML parser core
void Exp1::HTMLParser::HTMLParserCore(unsigned int& i, WString HTML, HTMLNode* Node)
{
	Exp1::WString Tmp1;
	unsigned int Status = HTMLNode::Text;

	//Child node chain of parent node
	HTMLNode __PBegin(HTMLNode::__Internal);
	HTMLNode* PREnd;

	if (Node->LNode)
	{
		__PBegin.RNode = PREnd = Node->LNode;
		while (PREnd->RNode)
			PREnd = PREnd->RNode;
	}
	else
		PREnd = &__PBegin;

	//Current element and its child node chain
	HTMLNode __CBegin(HTMLNode::__Internal);
	HTMLNode* CurrentElement = nullptr;
	HTMLNode* CREnd = nullptr;

	//HTML parser core
	while (i < HTML.Size())
		//Text
		if (Status == HTMLNode::Text)
		{	//"<"
			if (HTML[i] == L'<')
			{	//Save text node if text not empty
				if (!Tmp1.Empty())
				{
					HTMLNode* NewTextNode = new HTMLNode(HTMLNode::Text);
					NewTextNode->Value = Tmp1;
					PREnd->RNode = NewTextNode;
					PREnd = NewTextNode;
				}

				//"</"
				if (HTML[i + 1] == L'/')
				{
					//"i" can only be added one here
					//(Because another increment will happen outside this function call
					//before processing)
					i++;
					break;
				}

				Tmp1 = L"";
				//"<!"
				if (HTML[i + 1] == L'!')
				{
					while (HTML[i] != '>')
						i++;
					i++;
					continue;
				}

				//Begin an element
				//Do initialization
				i++;
				Status = HTMLNode::Element;
				CurrentElement = new HTMLNode(HTMLNode::Element);
				__CBegin.RNode = nullptr;
				CREnd = &__CBegin;
			}
			//Other characters
			else
			{
				Tmp1 += HTML[i];
				i++;
			}
		}
		//Element (name)
		else if (Status == HTMLNode::Element)
		{
			//" ", begin parsing properties
			if (HTML[i] == L' ')
				Status = HTMLNode::Prop;
			//">", recursively process inner HTML
			else if (HTML[i] == L'>')
			{
				//Process child nodes
				if (CREnd != &__CBegin)
					CurrentElement->LNode = __CBegin.RNode;
				Status = HTMLNode::__Internal;
				i++;

				//Recursive processing
				HTMLParserCore(i, HTML, CurrentElement);
			}
			//Other characters
			else
			{
				Tmp1 += HTML[i];
				//Element name fetched
				if ((HTML[i + 1] == ' ') || (HTML[i + 1] == '>'))
				{
					CurrentElement->Value = Tmp1;
					Tmp1 = L"";
				}
			}
			i++;
		}
		//Properties
		else if (Status == HTMLNode::Prop)
		{
			//Get property name
			while ((HTML[i] != L'=') && (HTML[i] != L'>') && (HTML[i] != L' '))
			{
				Tmp1 += HTML[i];
				i++;
			}

			//Create property node
			HTMLNode* NewProp = new HTMLNode(HTMLNode::Prop);
			NewProp->Value = Tmp1;
			CREnd->RNode = NewProp;
			CREnd = NewProp;

			//End of property part
			if (HTML[i] == L'>')
			{
				Status = HTMLNode::Element;
				continue;
			}

			//No property value
			if (HTML[i] == L' ')
			{
				i++;
				continue;
			}

			//Get property value
			Tmp1 = L"";
			wchar_t Delimiter = HTML[i + 1];
			i++;
			if ((Delimiter != L'\'') && (Delimiter != L'\"'))
				Delimiter = L' ';
			else
				i++;

			while (HTML[i] != Delimiter)
			{
				Tmp1 += HTML[i];
				i++;
			}
			//Create property value node
			HTMLNode* NewPropValue = new HTMLNode(HTMLNode::PropValue);
			NewPropValue->Value = Tmp1;
			NewProp->LNode = NewPropValue;

			if (HTML[i + 1] == L' ')
				i++;
			else if (HTML[i + 1] == L'>')
				Status = HTMLNode::Element;
			
			Tmp1 = L"";
			i++;
		}
		//Recover from recursive parsing (Pseudo "internal" state)
		else if (Status == HTMLNode::__Internal)
		{	
			unsigned int _i = i;
			//Read close tag name
			while (HTML[i] != L'>')
			{
				Tmp1 += HTML[i];
				i++;
			}

			//Add current element to chain regardless of whether open tag match close tag
			PREnd->RNode = CurrentElement;
			PREnd = CurrentElement;

			//Close tag match open tag, add to parent node
			if (Tmp1 == CurrentElement->Value)
			{
				//Cleaning works
				i++;
				Tmp1 = L"";
				Status = HTMLNode::Text;
			}
			//Not match, return to parent level
			else
			{
				//Same problem with line 66
				i = _i - 1;
				
				HTMLNode* CPropEnd = &__CBegin;
				__CBegin.RNode = CurrentElement->LNode;
				while ((CPropEnd->RNode) && (CPropEnd->RNode->Type == HTMLNode::Prop))
					CPropEnd = CPropEnd->RNode;

				if (CPropEnd == &__CBegin)
					CurrentElement->LNode = nullptr;
				if (CPropEnd->RNode)
				{
					PREnd->RNode = CPropEnd->RNode;
					CPropEnd->RNode = nullptr;
				}

				while (PREnd->RNode)
					PREnd = PREnd->RNode;

				break;
			}
		}

	//Append child node chain to parent node
	if (PREnd!=&__PBegin)
		Node->LNode = __PBegin.RNode;
}

//Destructor
Exp1::HTMLParser::~HTMLParser()
{
	HTMLParser::DestroyNodeTree(this->RootNode);
}

//Destroy HTML node tree
void Exp1::HTMLParser::DestroyNodeTree(HTMLNode* Node)
{
	//Recursively remove nodes
	if (Node->LNode)
		DestroyNodeTree(Node->LNode);
	if (Node->RNode)
		DestroyNodeTree(Node->RNode);
	delete Node;
}

//Iterate through a tree
void Exp1::HTMLParser::Iterate(HTMLNode* Node, std::function<void(HTMLNode*)> Func)
{
	Func(Node);
	if (Node->LNode)
		Iterate(Node->LNode, Func);
	if (Node->RNode)
		Iterate(Node->RNode, Func);
}

//Get elements by tag name
Exp1::Array<Exp1::HTMLElement> Exp1::HTMLParser::GetElementsByTagName(WString TagName)
{
	Array<HTMLElement> Result;
	//Iterate through the whole tree
	HTMLParser::Iterate(this->RootNode, [&](HTMLNode* Node)
	{
		if ((Node->Type == HTMLNode::Element) && (Node->Value == TagName))
			Result.Push(HTMLElement(this, Node));
	});

	return Result;
}

//Get elements by class name
Exp1::Array<Exp1::HTMLElement> Exp1::HTMLParser::GetElementsByClassName(WString ClassName)
{
	Array<HTMLElement> Result;
	HTMLNode* CurrentNode;

	//Iterate through the whole tree
	HTMLParser::Iterate(this->RootNode, [&](HTMLNode* Node)
	{
		//Record current node
		if (Node->Type == HTMLNode::Element)
			CurrentNode = Node;
		//Property detected, push current node to result
		if ((Node->Type == HTMLNode::Prop) && ((Node->Value) == L"class") && (Node->LNode) && (Node->LNode->Value == ClassName))
			Result.Push(HTMLElement(this, CurrentNode));
	});

	return Result;
}

#ifdef EXP1_DEBUG
//Recursively print all HTML nodes with proper indentation
void Exp1::HTMLParser::IteratePrint()
{
	__IteratePrint(this->RootNode, 0);
}

//Recursively print all HTML nodes with proper indentation (Core function)
void Exp1::HTMLParser::__IteratePrint(HTMLNode* Node, unsigned int Level)
{
	wchar_t TypeList[] = { L'T', L'P', L'V', L'E', L'I' };
	unsigned int i;

	for (i = 0; i < Level; i++)
		std::wcout << L' ';
	std::wcout << TypeList[Node->Type] << L' ' << Node->Value << std::endl;

	if (Node->LNode)
		__IteratePrint(Node->LNode, Level + 1);
	if (Node->RNode)
		__IteratePrint(Node->RNode, Level);
}
#endif