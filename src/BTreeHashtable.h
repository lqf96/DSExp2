#pragma once

#include "Util.h"
#include "LinkedList.h"

//Experiment II namespace
namespace Exp2
{
	//[ B-Tree Implementation ]
	//B-Tree M value
	const unsigned int __M = 8;

	//B-Tree hash table
	template <typename KT,typename VT>
	class BTreeHashTable
	{private:
		//[ Hashtable Structures ]
		//Key-value pair
		struct KVPair
		{
			KT Key;
			VT Value;
		};

		//B-Tree node
		struct BTreeNode
		{
			//Size of hash list
			unsigned int Size;
			//Hash list
			unsigned int HashList[__M - 1];
			//Data list
			Exp1::LinkedList<KVPair> DataList[__M - 1];
			//Child nodes list
			BTreeNode* Children[__M];
			//Leaf node indicator
			bool Leaf;
		};

		//[ Member Variables ]
		//Tree root
		BTreeNode* Root;
		//Hash function
		std::function<unsigned int(KT)> HashFunc;
		//Reference count
		unsigned int* RefCount;
		//Hash table size
		unsigned int __Size = 0;

		//[ Static Members ]
		//Empty data list
		static Exp1::LinkedList<KVPair> EmptyDataList;
		//Empty object (For indicating no existance in hash table)
		static VT EmptyObj;

		//[ B-Tree Operations ]
		//Split a full parent node at a given position
		static void __SplitChild(BTreeNode* Node, unsigned int Position)
		{
			//Create a new node
			BTreeNode* NewNode = new BTreeNode;
			BTreeNode* ChildNode = Node->Children[Position];
			//Loop counter
			int i = 0;

			//Initialize new node and copy child node's information
			NewNode->Leaf = ChildNode->Leaf;
			NewNode->Size = __M / 2 - 1;

			//Copy hash values and data lists to new node
			for (i = 0; i < __M / 2 - 1; i++)
			{
				NewNode->HashList[i] = ChildNode->HashList[i + __M / 2];
				NewNode->DataList[i] = ChildNode->DataList[i + __M / 2];
			}
			//Then change child node's size
			ChildNode->Size = __M / 2 - 1;

			//Copy part of child node's children to new node if it isn't leaf
			if (!ChildNode->Leaf)
				for (i = 0; i <= __M / 2 - 1; i++)
					NewNode->Children[i] = ChildNode->Children[i + __M / 2];

			//Move hash values, data lists and children
			for (i = Node->Size; i > Position; i--)
			{
				Node->HashList[i] = Node->HashList[i - 1];
				Node->DataList[i] = Node->DataList[i - 1];
				Node->Children[i + 1] = Node->Children[i];
			}

			//Add new hash value and child node
			Node->Children[Position + 1] = NewNode;
			Node->HashList[Position] = ChildNode->HashList[__M / 2 - 1];
			Node->DataList[Position] = ChildNode->DataList[__M / 2 - 1];
			//Update size
			Node->Size++;
		}

		//Insert hash value to a non-null node
		static void __InsertNonFull(BTreeNode* Node, KVPair DataPair, unsigned int Hash)
		{
			//Counter
			int i = Node->Size - 1;

			//Leaf node
			if (Node->Leaf)
			{
				//Find a place to insert hash value and data pair
				while ((i >= 0) && (Hash < Node->HashList[i]))
				{
					Node->HashList[i + 1] = Node->HashList[i];
					Node->DataList[i + 1] = Node->DataList[i];
					i--;
				}

				//Update node information
				Node->HashList[i + 1] = Hash;
				Node->DataList[i + 1] = Exp1::LinkedList<KVPair>();
				Node->DataList[i + 1].Push(DataPair);
				Node->Size++;
			}
			//Non-leaf node
			else
			{
				//Find a place to insert hash value and data pair
				while ((i >= 0) && (Hash < Node->HashList[i]))
					i--;
				i++;

				//Split node when necessary
				if (Node->Children[i]->Size == __M - 1)
				{
					__SplitChild(Node, i);
					if (Hash > Node->HashList[i])
						i++;
				}

				//Do recursive insertion
				__InsertNonFull(Node->Children[i], DataPair, Hash);
			}
		}

		//B-Tree insertion
		static void Insert(BTreeNode*& Node, KVPair Pair, unsigned int Hash)
		{
			//Root node is full, split root node
			if (Node->Size == __M - 1)
			{
				//Create new node
				BTreeNode* NewNode = new BTreeNode;
				NewNode->Leaf = false;
				NewNode->Size = 0;
				NewNode->Children[0] = Node;

				//Substitute tree root
				Node = NewNode;
				//Split root node
				__SplitChild(NewNode, 0);
				//Insert hash value and data
				__InsertNonFull(NewNode, Pair, Hash);
			}
			//Not null, directly do insertion
			else
				__InsertNonFull(Node, Pair, Hash);
		}
		
		//B-Tree searching (Return corresponding Key-Value Pair)
		static Exp1::LinkedList<KVPair>& Search(BTreeNode* Node, unsigned int Hash, bool* Found)
		{
			unsigned int i = 0;

			//Traverse in the hash list
			while ((i < Node->Size) && (Hash > Node->HashList[i]))
				i++;

			//Hash value found
			if ((i < Node->Size) && (Hash == Node->HashList[i]))
			{
				*Found = true;
				return Node->DataList[i];
			}
			//Leaf node (No possiblity for finding the node)
			else if (Node->Leaf)
			{
				*Found = false;
				return BTreeHashTable<KT, VT>::EmptyDataList;
			}
			//Do recursive searching
			else 
				return Search(Node->Children[i], Hash, Found);
		}
		
		//Tree cloning
		static BTreeNode* CloneTree(const BTreeNode* Node)
		{
			unsigned int i;

			//Clone current node
			BTreeNode* NewNode = new BTreeNode(*Node);
			//Clone child nodes
			if (!Node->Leaf)
				for (i = 0; i <= Node->Size; i++)
					NewNode->Children[i] = CloneTree(Node->Children[i]);
			//Return new node
			return NewNode;
		}
		
		//Tree Destroying
		static void DeleteTree(BTreeNode* Node)
		{
			//Non-leaf node, destroy children
			if (!Node->Leaf)
			{
				unsigned int i;
				for (i = 0; i <= Node->Size; i++)
					delete Node->Children[i];
			}
			//Destroy self
			delete Node;
		}
		
		//Tree traversal helper
		static void TraverseIA(BTreeNode* Node, std::function<void(KT, VT)> Func)
		{
			unsigned int i;
			//Traverse in hash list
			for (i = 0; i < Node->Size; i++)
				TraverseII(Node->DataList[i], Func);
			//For non-leaf node, traverse through children
			if (!Node->Leaf)
				for (i = 0; i <= Node->Size; i++)
					TraverseIA(Node->Children[i], Func);
		}
		
		//Tree traversal helper (Traverse in key-value list)
		static void TraverseII(Exp1::LinkedList<KVPair>& List, std::function<void(KT, VT)> Func)
		{
			//Traverse in the data list
			List.ForEach([&](KVPair DataPair)
			{
				Func(DataPair.Key, DataPair.Value);
			});
		}

		//[ Member Methods ]
		//Clone internal tree
		void Clone()
		{
			//New internal tree
			this->Root = CloneTree(this->Root);

			//Update reference count
			(*this->RefCount)--;
			//Set new reference count
			this->RefCount = new unsigned int(1);
		}
	public:
		//[ Member Methods ]
		//Constructor
		BTreeHashTable(std::function<unsigned int(KT)> _HashFunc)
		{
			this->HashFunc = _HashFunc;
			this->RefCount = new unsigned int(1);
			this->__Size = 0;

			//Root node initialization
			this->Root = new BTreeNode;
			this->Root->Size = 0;
			this->Root->Leaf = true;
		}
		
		//Simple constructor (Use KT::Hash as hash function)
		BTreeHashTable() : BTreeHashTable(Exp1::WrapMemberFunc(&KT::Hash)) {}

		//Copy constructor
		BTreeHashTable(const BTreeHashTable<KT, VT>& Another)
		{
			this->Root = nullptr;
			this->RefCount = nullptr;

			//Use "=" operator when copying
			*this = Another;
		}
		
		//Destructor
		~BTreeHashTable()
		{
			(*this->RefCount)--;
			//No reference pointing to internal tree
			if (*this->RefCount == 0)
			{
				DeleteTree(this->Root);
				delete this->RefCount;
			}
		}

		//Overload operator "=" for array
		BTreeHashTable<KT, VT>& operator =(const BTreeHashTable<KT, VT>& Another)
		{
			//Copy size
			this->__Size = Another.__Size;
			//Copy hash function
			this->HashFunc = Another.HashFunc;

			//Internal tree not same
			if (this->RefCount != Another.RefCount)
			{
				//Only one count to internal tree, destroy internal tree and reference count
				if ((this->RefCount) && (*this->RefCount == 1))
				{
					DeleteTree(this->Root);
					delete this->RefCount;
				}

				//Copy internal tree and reference count
				this->Root = Another.Root;
				this->RefCount = Another.RefCount;
				//Update reference count
				(*this->RefCount)++;
			}

			return *this;
		}

		//Put key-value pair
		void Put(KT Key, VT Value)
		{
			unsigned int Hash = this->HashFunc(Key);
			//Clone the tree when necessary
			if (*this->RefCount > 1)
				this->Clone();

			//Try searching for data list with given hash value
			bool Found;
			Exp1::LinkedList<KVPair>& DataList = Search(this->Root, Hash, &Found);
			//Node found
			if (Found)
			{
				//Search in the data list
				auto Ptr = DataList.Begin();
				while (Ptr != DataList.End())
				{
					//Key found, modify its value
					if (Ptr->Key == Key)
					{
						Ptr.Data().Value = Value;
						return;
					}
					Ptr++;
				}

				//Key not found, create a new key-value pair
				KVPair DataPair;
				DataPair.Key = Key;
				DataPair.Value = Value;
				DataList.Push(DataPair);
				this->__Size++;
			}
			//Not found
			else
			{
				KVPair DataPair;
				DataPair.Key = Key;
				DataPair.Value = Value;
				Insert(this->Root, DataPair, Hash);
				this->__Size++;
			}
		}

		//Get value from key (Read-only access)
		const VT& Get(KT Key, bool* Exists = nullptr) const
		{
			unsigned int Hash = this->HashFunc(Key);

			//Try searching for data list with given hash value
			bool Found;
			auto DataList = Search(this->Root, Hash, &Found);
			//Node found
			if (Found)
			{
				auto Ptr = DataList.Begin();
				while (Ptr != DataList.End())
				{
					if (Ptr->Key == Key)
					{
						if (Exists)
							*Exists = true;
						return Ptr->Value;
					}
				}
			}

			//Key not found, return default value
			if (Exists)
				*Exists = false;
			return BTreeHashTable<KT, VT>::EmptyObj;
		}

		//Remove element (Not implemented)
		
		//Check if the hashtable contains a specific key
		bool Contains(KT Key) const
		{
			bool __Contains;
			this->Get(Key, &__Contains);
			return __Contains;
		}

		//Override operator "[]" for read-only access
		const VT& operator [](KT Key) const
		{
			return this->Get(Key);
		}

		//Read write access to hash table
		VT& Data(KT Key, bool* Exists = nullptr)
		{
			if (*this->RefCount > 1)
				this->Clone();
			return const_cast<VT&>(this->Get(Key, Exists));
		}

		//Get size of the hash table
		unsigned int Size() const
		{
			return this->__Size;
		}

		//Check if the hash table is empty
		bool Empty() const
		{
			return this->__Size == 0;
		}

		//Traverse through the hash table (Preorder traversal)
		void ForEach(std::function<void(KT, VT)> Func) const
		{
			TraverseIA(this->Root, Func);
		}
	};

	//[ B-Tree Hash Table Static Members ]
	//Empty data list
	template <typename KT, typename VT>
	Exp1::LinkedList<typename BTreeHashTable<KT, VT>::KVPair> BTreeHashTable<KT, VT>::EmptyDataList;
	//Empty object (For indicating no existance in hash table)
	template <typename KT, typename VT>
	VT BTreeHashTable<KT, VT>::EmptyObj;
}
