#pragma once
#include <cstring>
#include <functional>
#include <iostream>
#include <sys/stat.h>
#include "Array.h"
#include "String.h"

//Unix systems
#ifdef UNIX
#include <dirent.h>
//Windows
#else
#include <io.h>
#endif

//Experiment 1 namespace
namespace Exp1
{
	//[ File Utilities ]
	//Get the name of the files in a dictionary
	Array<String> GetFiles(String Folder, String FileNameFilter);

	//[ Function Utilities ]
	//Wrap member function as a normal function
	template <typename CT, typename MFReturnType, typename... MFArgType>
	std::function<MFReturnType(CT, MFArgType...)> WrapMemberFunc(MFReturnType(CT::*MemberFunc)(MFArgType...))
	{
		return [MemberFunc](CT Instance, MFArgType... Args) -> MFReturnType
		{
			return (Instance.*MemberFunc)(Args...);
		};
	}

	//[ Hashtable Utilities ]
	//Hash for unsigned int
	unsigned int UIntHash(unsigned int UInt);

	//[ C++ Utilities ]
	//Run code in declration (Implementation)
	class __ImmediateRunCode
	{public:
		__ImmediateRunCode(std::function<void()> CodeFunc)
		{
			CodeFunc();
		}
	};

	//Run code in declration (C++ Marco)
	#define DECLARE_IMMEDIATE_CODE(Name)				extern Exp1::__ImmediateRunCode __IRC_##Name;
	#define BEGIN_IMMEDIATE_CODE(Name)					Exp1::__ImmediateRunCode __IRC_##Name([](){
	#define BEGIN_IMMEDIATE_CODE_NS(Namespace, Name)	Exp1::__ImmediateRunCode Namespace::__IRC_##Name([](){
	#define END_IMMEDIATE_CODE							});

	//[ Binary Stream Utilities ]
	//Write data to stream
	template <typename T>
	inline void WriteData(std::basic_ostream<char>& Out, const T& Data)
	{
		const char* DataPtr = (const char*)(&Data);
		Out.write(DataPtr, sizeof(T));
	}

	//Read data from binary stream
	template <typename T>
	inline T ReadData(std::basic_istream<char>& In)
	{
		T Data;
		In.read((char*)(&Data), sizeof(T));
		return Data;
	}

	//Read string from binary stream
	inline String ReadString(std::basic_istream<char>& In, unsigned int Length)
	{
		String Result;
		unsigned int i;

		for (i = 0; i < Length; i++)
			Result.Push(In.get());
		return Result;
	}

	//[ File Stat Utilities ]
	//File stat type
#ifdef UNIX
	typedef struct ::stat FStat;
#else
	typedef struct ::_stat FStat;
#endif

	//Stat function
	inline int Stat(const char* PathName, FStat* Buffer)
	{
#ifdef UNIX
		return stat(PathName, Buffer);
#else
		return _stat(PathName, Buffer);
#endif
	}
}
